Change Log Directions

In directory: ..\PinPoint3\pinpoint3.webprojects\default.webproject

   * Copy over all images
	- BackIcon.png, ExpandMenuIcon.png, FormIcon.png, HideMenuIcon.png, LoginIcon.png, LoginUsernameIcon.png
	- Replace LoginUsernameIcon.png and LoginIcon.png with the files of the same name
   * Remove TruTrade_reversed-logo.png and TT_logo.png as they are not in use for this project
   
   * Replace any placeholder images currently in use for BackIcon.png, ExpandMenuIcon.png, FormIcon.png, HideMenuIcon.png
     within Alpha Anywhere. The use of these images can be seen in the Maps Screen mockup in the dev files.

   