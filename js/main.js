/* Set the width of the side navigation to 300px and the left margin of the page content to 300px */
document.getElementById("mySidenav").style.width = "310px";
document.getElementById("main").style.marginLeft = "310px";
function openNav() {
  document.getElementById("mySidenav").style.width = "310px";
  document.getElementById("main").style.marginLeft = "310px";

  var d = document.getElementById("basemapbtns");
  d.style.position = "absolute";
  d.style.left = "370px";

  var d = document.getElementById("map_zoom_slider");
  d.style.position = "absolute";
  d.style.top = "20px";

  var d = document.getElementById("birdsEyebtn");
  d.style.position = "absolute";
  d.style.top = "120px";
  d.style.left = "330px";

  var d = document.getElementById("printbtn");
  d.style.position = "absolute";
  d.style.top = "190px";
  d.style.left = "330px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft = "0";

  var d = document.getElementById("basemapbtns");
  d.style.position = "absolute";
  d.style.left = "60px";
  var d = document.getElementById("map_zoom_slider");
  d.style.position = "absolute";
  d.style.top = "60px";

  var d = document.getElementById("birdsEyebtn");
  d.style.position = "absolute";
  d.style.top = "160px";
  d.style.left = "20px";
  var d = document.getElementById("printbtn");
  d.style.position = "absolute";
  d.style.top = "230px";
  d.style.left = "20px";

  //updateMap()
}
//NOW HANDLE LEGEND
//document.getElementById("legendDIV").style.height = "510px";
$(".leg_titlebar").on("click", function () {
  $("#legclose i").toggleClass("fa-angle-double-up fa-angle-double-down");
  $(".legendDiv").toggleClass("open closed");
});

// Load MAPS
var map,
  infos = {},
  dynamicLayerInfos,
  extentInitial,
  centersFL,
  compCentersFL,
  legend,
  gsvc,
  compOverlayControl,
  blockGroupWinControl,
  zipCodeControl,
  selectedPrimaryIDs = [],
  heatmapFeatureLayer,
  primaryGlobalID = [];
var streetView, birdsEyeView, zoomOutHandler, zoomInHandler;
var isCoordsTool = false,
  isCoordsZoom = false,
  xCoord,
  yCoord,
  coordsPoint;
require([
  "rok/config",
  "esri/map",
  "src/js/bootstrapmap.js",
  "esri/dijit/HomeButton",
  "esri/layers/ArcGISDynamicMapServiceLayer",
  "esri/layers/FeatureLayer",
  "esri/dijit/Legend",
  "esri/tasks/query",
  "esri/tasks/QueryTask",
  "esri/layers/LabelClass",
  "esri/dijit/PopupTemplate",
  "ncam/PopupExtended",
  "esri/layers/GraphicsLayer",
  "esri/Color",
  "rok/rokTOC",
  "rok/mediaCoverageLayers",
  "rok/dynamicShopperOriginLayer",
  "rok/createCompetitorOverlayLayer",
  "rok/centerPenetrationLayer",
  "rok/competitorPenetrationLayer",
  "rok/rokHotSpotLayer",
  "rok/zipCodeLayer",
  "esri/layers/LayerDrawingOptions",
  "esri/layers/TableDataSource",
  "esri/layers/LayerDataSource",
  "esri/renderers/UniqueValueRenderer",
  "esri/renderers/SimpleRenderer",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/tasks/ProjectParameters",
  "esri/SpatialReference",
  "esri/tasks/GeometryService",
  "esri/tasks/PrintTask",
  "esri/tasks/PrintParameters",
  "esri/tasks/PrintTemplate",
  "dojo/_base/array",
  "dojo/domReady!",
], function (
  config,
  Map,
  BootstrapMap,
  HomeButton,
  ArcGISDynamicMapServiceLayer,
  FeatureLayer,
  Legend,
  Query,
  QueryTask,
  LabelClass,
  PopupTemplate,
  PopupExtended,
  GraphicsLayer,
  Color,
  rokTOC,
  mediaLayersCreate,
  CenterDynamicShopperOriginLayer,
  createCompetitorOverlayLayer,
  CenterDynamicPenetrationLayer,
  CompetitorDynamicPenetrationLayer,
  rokHotSpotLayer,
  zipCodeLayer,
  LayerDrawingOptions,
  TableDataSource,
  LayerDataSource,
  UniqueValueRenderer,
  SimpleRenderer,
  SimpleFillSymbol,
  SimpleLineSymbol,
  ProjectParameters,
  SpatialReference,
  GeometryService,
  PrintTask,
  PrintParameters,
  PrintTemplate,
  arrayUtils
) {
  compOverlayControl = createCompetitorOverlayLayer;
  blockGroupWinControl = CenterDynamicShopperOriginLayer;
  zipCodeControl = zipCodeLayer;
  var layerDefinitions = [];
  var masterUser = true;
  var compTadeAreaColorList = [
    "#66FF66",
    "#a900e6",
    "#ff8c00",
    "#ffff66",
    "#ff0000",
    "#0070ff",
    "#ffa77f",
    "#5e7a19",
    "#12dbff",
    "#a80000",
    "#66FF66",
    "#a900e6",
    "#ff8c00",
    "#ffff66",
    "#ff0000",
    "#0070ff",
    "#ffa77f",
    "#5e7a19",
    "#12dbff",
    "#a80000",
    "#66FF66",
    "#a900e6",
    "#ff8c00",
    "#ffff66",
    "#ff0000",
    "#0070ff",
    "#ffa77f",
    "#5e7a19",
    "#12dbff",
    "#a80000",
    "#66FF66",
    "#a900e6",
    "#ff8c00",
    "#ffff66",
    "#ff0000",
    "#0070ff",
    "#ffa77f",
    "#5e7a19",
    "#12dbff",
    "#a80000",
  ];

  var fillSymbolC = new SimpleFillSymbol();

  gsvc = new GeometryService(
    "https://arcgis4.roktech.net/arcgis/rest/services/Utilities/Geometry/GeometryServer"
  );
  gsvc.on("project-complete", configureMapClickCoords); //Project point from 102719 to 4326 to use with Bing and Google
  map = BootstrapMap.create("map", {
    basemap: "streets-vector", // Full list of pre-defined basemaps: http://arcg.is/1JVo6Wd
    center: config.center,
    zoom: config.zoom,
    showLabels: true,
  });

  map.on("load", addHomeSlider);
  function addHomeSlider() {
    //let's add the home button slider as a created class, requrires dom-Attr
    dojo.create(
      "div",
      {
        className: "esriSimpleSliderHomeButton",
        title: "Zoom to Full Extent",
        onclick: function () {
          if (extentInitial === undefined) {
            extentInitial = map.extent;
          }
          map.setExtent(extentInitial);
        },
      },
      dojo.query(".esriSimpleSliderIncrementButton")[0],
      "after"
    );
  }
  //Operational Layers
  operationalMapService = new ArcGISDynamicMapServiceLayer(
    config.operationalMapService,
    { id: "operationalMapService", dynamicLayerInfos: [] }
  );
  operationalMapService.setOpacity(0.8);
  operationalMapService.setVisibleLayers([]);
  map.on("load", function (evt) {
    legend = new Legend(
      {
        map: map,
        layerInfos: [],
      },
      "legendDynamicLayers"
    );
    legend.startup();
  });

  $("#legendDynamicLayers").hide();
  //Centers & Competitors
  var centersPopTemplate = new PopupTemplate({
    title: "Location Info",
    fieldInfos: [
      { fieldName: "CenterName", label: "Center Name", visible: true },
      { fieldName: "Address", label: "Address", visible: true },
      { fieldName: "City", label: "City", visible: true },
      { fieldName: "State", label: "State", visible: true },
      { fieldName: "Zip", label: "Zip", visible: true },
      { fieldName: "Latitude", label: "Latitude", visible: true },
      { fieldName: "Longitude", label: "Longitude", visible: true },
      { fieldName: "MetroID", label: "MetroID", visible: true },
      { fieldName: "CompanyID", label: "CompanyID", visible: true },
      { fieldName: "SquareFeet", label: "Square Feet", visible: true },
      { fieldName: "CenterManager", label: "Center Manager", visible: true },
      { fieldName: "CenterOwner", label: "Center Owner", visible: true },
      {
        fieldName: "TypeOfCenterID",
        label: "Type Of Center ID",
        visible: true,
      },
      { fieldName: "SiteID", label: "SiteID", visible: true },
      { fieldName: "POIID", label: "POIID", visible: true },
    ],
    extended: {
      actions: [
        {
          text: "Zoom to",
          className: "defaultAction",
          title: "Zoom to selected feature.",
          click: function (feature) {
            zoomtoFromInfoWindow(feature);
          },
        },
      ],
      themeClass: "extended", //uses a pretty bad custom theme defined in PopupExtended.css.
      scaleSelected: 0,
    },
  });
  centersFL = new FeatureLayer(
    config.operationalMapService + "/" + config.centerLayerID,
    { id: "centersFLID", outFields: ["*"], infoTemplate: centersPopTemplate }
  );

  var competitorsPopTemplate = new PopupTemplate({
    title: "Competitor Info",
    fieldInfos: [
      { fieldName: "CompetitorName", label: "Competitor Name", visible: true },
      { fieldName: "City", label: "City", visible: true },
      { fieldName: "State", label: "State", visible: true },
      { fieldName: "Latitude", label: "Latitude", visible: true },
      { fieldName: "Longitude", label: "Longitude", visible: true },
      { fieldName: "CompetitorID", label: "CompetitorID", visible: true },
      { fieldName: "CompanyID", label: "CompanyID", visible: true },
      { fieldName: "SquareFeet", label: "Square Feet", visible: true },
      { fieldName: "CenterManager", label: "Center Manager", visible: true },
      { fieldName: "CenterOwner", label: "Center Owner", visible: true },
      {
        fieldName: "TypeOfCenterID",
        label: "Type Of Center ID",
        visible: true,
      },
      { fieldName: "SiteID", label: "SiteID", visible: true },
      { fieldName: "POIID", label: "POIID", visible: true },
    ],
    extended: {
      actions: [
        {
          text: "Zoom to",
          className: "defaultAction",
          title: "Zoom to selected feature.",
          click: function (feature) {
            zoomtoFromInfoWindow(feature);
          },
        },
      ],
      themeClass: "extended", //uses a pretty bad custom theme defined in PopupExtended.css.
      scaleSelected: 0,
    },
  });
  compCentersFL = new FeatureLayer(
    config.operationalMapService + "/" + config.competitorCenterLayerID,
    {
      id: "compCentersFLID",
      outFields: ["*"],
      infoTemplate: competitorsPopTemplate,
    }
  );

  //Create an extended Popup.
  //The exteneded object contains the custom properties applicable only to PopupExtended. Any other normal popup properties can be defined as per normal (not included in the extended object though).
  var extendedPopup = new PopupExtended(
    {
      extended: {
        themeClass: "light",
        draggable: true,
        defaultWidth: 290,
        hideOnOffClick: true,
        multiple: false,
        smallStyleWidthBreak: 100, //was 768, changed so it would show mobile
      },
      highlight: false,
    },
    dojo.create("div")
  );
  function zoomtoFromInfoWindow(feature) {
    map.infoWindow.hide();
    map.centerAndZoom(feature.geometry, 19);
  }
  //set the map to use the exteneded popup
  extendedPopup.setMap(map);
  map.infoWindow = extendedPopup;

  var selectgraphic = new GraphicsLayer();
  var zoomGraphics = new GraphicsLayer();
  map.addLayers([
    compCentersFL,
    centersFL,
    selectgraphic,
    zoomGraphics,
    operationalMapService,
  ]);

  $("#transSlider").on("input oninput", function (e) {
    var transparency = parseFloat(e.target.value);
    operationalMapService.setOpacity(transparency);
    centersFL.setOpacity(transparency);
    compCentersFL.setOpacity(transparency);
  });

  $("#tvCoverageBtn").click(function () {
    $("#tvDIV").toggle();
    $("#tvCoverageBtn i").toggleClass("fa-sort-up fa-sort-down");
  });
  $("#radioCoverageBtn").click(function () {
    $("#radioDIV").toggle();
    $("#radioCoverageBtn i").toggleClass("fa-sort-up fa-sort-down");
  });
  centersFL.on("load", function (evt) {
    setTimeout(function (e) {
      rokHotSpotLayer.createHotSpotLayer();
      // mediaLayersCreate.createTVRadioLayers(map, selectedPrimaryIDs, 0);
    }, 3000);
  });
  centersFL.on("visibility-change", function (evt) {
    console.log(evt);
  });
  compCentersFL.on("visibility-change", function (evt) {
    console.log(evt);
  });
  operationalMapService.on("update-start", function () {
    $("#loading").show();
  });
  operationalMapService.on("update-end", function () {
    $("#loading").hide();
  });
  operationalMapService.on("load", function (e) {
    dynamicLayerInfos = e.target.createDynamicLayerInfosFromLayerInfos();
    arrayUtils.forEach(dynamicLayerInfos, function (info) {
      if (info.id !== 0 || info.id !== 1) {
        var i = {
          id: info.id,
          name: info.name,
          position: info.id,
          visible: false,
        };
        infos[info.id] = i;
      }
    });
    infos.total = dynamicLayerInfos.length;
    e.target.setDynamicLayerInfos(dynamicLayerInfos, true);
    operationalMapService.setDynamicLayerInfos([]);
    rokTOC.createToc();
  });

  //Handle map base maps
  $("#basemapbtns button").click(function (e) {
    $("#basemapbtns button").removeClass("active");
    $(this).addClass("active");
    switch (e.target.title) {
      case "Streets":
        map.setBasemap("streets-vector");
        break;
      case "Streets - Night":
        map.setBasemap("streets-night-vector");
        break;
      case "Imagery":
        map.setBasemap("hybrid");
        break;
      case "National Geographic":
        map.setBasemap("national-geographic");
        break;
      case "Topographic":
        map.setBasemap("topo-vector");
        break;
      case "Open Street Map":
        map.setBasemap("osm");
        break;
    }
  });
  //GET URL PARAMETERS
  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(
      /[?&]+([^=&]+)=([^&]*)/gi,
      function (m, key, value) {
        vars[key] = value;
      }
    );
    return vars;
  }
  /**/
  $(".leg_titlebar").on("click", function () {
    $(".legendDIV").toggleClass("closed open");
  });
  //URL PARAMS
  // var urlCenterID = getUrlVars()["CenterID"];
  var primaryGlobalID = getUrlVars()["PrimaryGlobalID"];
  var userID = getUrlVars()["UserID"];
  var userRole = getUrlVars()["UserRole"];
  selectedCenterPOI = [getUrlVars()["POI"]];

  checkCookie();
  var first = true;
  function checkCookie() {
    if (userID == 491) {
      $("#dashboardbtn").hide();
    }
    if (userID) {
      getCenters();
      $("#jpgsTool").show();
      if (userRole == 1) {
        masterUser = true;
      }
    }
  }

  _sizeWidgetPanel();
  function _sizeWidgetPanel($window, $header, $panel, $bottomRow) {
    // could potentially exploit map height for this calc
    // sizes the widget panel to the browser height to facilitate scrolling
    setTimeout(function () {
      var windowHeight = $(window);
      $("#main").height(windowHeight);
      $("#map").height(windowHeight);
      map.resize();
    }, 3000);
  }

  var centerID = [],
    locationIDs = [],
    selectedCenterPOIID = [],
    competitorsForDefQUery = [],
    competitorsForRender = [],
    centersForRenderer = [];
  var allCenters = [],
    allCompetitors = [];
  function getCompetorIds() {
    if (masterUser == true) {
      $("#advFunctions").show();
      $("#zipCodeDiv").show();
      $("#zipCodeDiv").removeClass("layercreated");
      $("#zipCodeChkDiv").hide();
      $("#mktPenDiv").show();
      $("#mktPenDiv").removeClass("layercreated");
      $("#mktPenChkDiv").hide();
      $("#mktCompPenDiv").show();
      $("#mktCompPenDiv").removeClass("layercreated");
      $("#mktCompPenChkDiv").hide();
      //Expenditures
      $("#expenditureChkDiv").hide();
      $("#expenditureBtn").removeClass("btn-info");
      $("#expenditureBtn").addClass("btn-primary");
    }
    $("#blkGroupDiv").show();
    $("#blkGroupDiv").removeClass("layercreated");
    $("#blkGroupChkDiv").hide();
    $("#competitorOverlayDiv").show();
    $("#competitorOverlayDiv").removeClass("layercreated");
    $("#competitorOverlayChkDiv").hide();
    $("#heatmapDiv").show();
    $("#heatmapDiv").removeClass("layercreated");
    $("#heatmapChkDiv").hide();
    selectedCenter = [];
    selectedCenter2 = [];
    var lsRegExp = /'/g;
    var multiSelect = $("#locationIDs")[0].children;
    for (var index = 0; index < multiSelect.length; index++) {
      if (multiSelect[index].selected) {
        selectedCentercount = selectedCentercount + 1;
        var centerName = String(multiSelect[index].text).replace(
          lsRegExp,
          "''"
        );
        selectedCenter.push("'" + centerName + "'");
        selectedCenter2.push(centerName);
        selectedPrimaryIDs.push(multiSelect[index].id);
      }
    }
    if (masterUser == true && selectedCentercount == 1) {
      $("#centerShopperDiv").show();
      $("#competitorShopperDiv").show();
      $("#mktCenterPenDiv").show();
      $("#mktCompPenDiv").show();
      //Media Coverage
      $("#mediaCoverageDiv").show();
      $("#mediaCoverageChkDiv").hide();
    }
    // ajax call that hits the filterFromCenterSelection endpoint
    $.ajax({
      method: "POST",
      url:
        "https://trutradedevelop.alexanderbabbage.com:5000/maps-develop/filterFromSelectedCenter",
      data: { userId: userID, centerId: selectedPrimaryIDs },
    })
      .done(function (res) {
        $("#competitorsID").empty();
        //Populate Drop down listof available Competitors
        var competitors = [];
        centerID = [];
        for (var i = 0; i < res.length; i++) {
          centerID.push("'" + res[i].PrimaryGlobalID + "'");
          centersForRenderer.push(res[i].PrimaryName);
          centersForPenetration.push(res[i].PrimaryName);
          competitorsForRender.push(
            res[i].CompetitorName.replace(/\s+$/g, "").trim()
          );
          var competitorName = String(res[i].CompetitorName)
            .replace(/'/g, "''")
            .trim();
          compCenters.push("'" + res[i].CompetitorGlobalID + "'");
          competitors.push(
            "<option name='" +
              res[i].CompetitorName +
              "' value='" +
              res[i].CompetitorGlobalID +
              "'>" +
              res[i].CompetitorName +
              "</option>"
          );
          competitorsForPenetration.push("'" + competitorName.trim() + "'");
        }

        var uniquecompetitorsCenters = [];
        $.each(competitors, function (i, el) {
          if ($.inArray(el, uniquecompetitorsCenters) === -1)
            uniquecompetitorsCenters.push(el);
        });
        $("#competitorsID").append(uniquecompetitorsCenters.sort());
        $("#competitorsID").multiselect("rebuild");
        $("#competitorsID").multiselect({
          numberDisplayed: 2,
          includeSelectAllOption: true,
          selectAllValue: "Show All",
        });
        $("#competitorsID").multiselect("selectAll", false);
        $("#competitorsID").multiselect("updateButtonText");
        $("#competitorsID").multiselect("enable");
        //Set Layer Definition
        getMaxDeviceVal(
          selectedCenter,
          competitorsForDefQUery,
          competitorsForRender,
          selectedPrimaryIDs
        );

        zoomToExtentofCompetitors(selectedPrimaryIDs);
      })
      .fail(function (jqXHR, textStatus) {
        console.error("Request Failed " + textStatus);
      });
  }
  function zoomToExtentofCompetitors(id) {
    var query = new Query();
    var queryTask = new QueryTask(
      config.operationalMapService + "/" + config.centerLayerID
    );
    query.where = "GLOBALID in (" + "'" + id + "'" + ")";
    query.returnGeometry = true;
    query.outFields = ["OBJECTID"];
    queryTask.execute(query, function (results) {
      // automatically grabs the last lat and lon in the results.features array.
      // the last value in the array equals the SelectedCenterID
      map.centerAndZoom(results.features[0].geometry, 9);
      if (first == true) {
        setTimeout(function () {
          extentInitial = map.extent.expand(1.5);
        }, 3000);
      }
    });
  }

  // uncomment for testing

  function getCenters() {
    // ajax call the hits the getCenterIds endpoint
    $.ajax({
      method: "POST",
      url:
        "https://trutradedevelop.alexanderbabbage.com:5000/maps-develop/getIdList/",
      data: { userId: userID, primaryGlobalId: primaryGlobalID },
    })
      .done(function (res) {
        //Create Layerlist
        //Populate Drop down listof available Competitors
        var centers = [],
          competitors = [];
        var lsRegExp = /'/g;
        var c = primaryGlobalID;
        for (var i = 0; i < res.centers.length; i++) {
          var centerName = String(res.centers[i].CenterName).replace(
            lsRegExp,
            "''"
          );
          centerID.push("'" + res.centers[i].primaryglobalid + "'");
          locationIDs.push("'" + res.centers[i].primaryglobalid + "'");
          if (c === res.centers[i].primaryglobalid) {
            centers.push(
              "<option name='" +
                res.centers[i].CenterName +
                "' id='" +
                res.centers[i].primaryglobalid +
                "' selected>" +
                res.centers[i].CenterName +
                "</option>"
            );
          } else {
            centers.push(
              "<option name='" +
                res.centers[i].CenterName +
                "' id='" +
                res.centers[i].primaryglobalid +
                "'>" +
                res.centers[i].CenterName +
                "</option>"
            );
          }
        }
        for (var i = 0; i < res.competitors.length; i++) {
          var competitorName = String(res.competitors[i].CenterName).replace(
            lsRegExp,
            "''"
          );
          competitorsForDefQUery.push("'" + res.competitors[i].globalid + "'");
          competitors.push(
            "<option name='" +
              res.competitors[i].CenterName +
              "' value='" +
              res.competitors[i].globalid +
              "'>" +
              res.competitors[i].CenterName +
              "</option>"
          );
        }
        var uniqueCenters = [];
        $.each(centers, function (i, el) {
          if ($.inArray(el, uniqueCenters) === -1) uniqueCenters.push(el);
        });
        $("#locationIDs").append(uniqueCenters.sort());
        $("#locationIDs").multiselect({
          numberDisplayed: 2,
          includeSelectAllOption: false,
          selectAllValue: "Show All",
          onChange: function (option, checked) {
            if ($("#locationIDs option:selected").length === 0) {
              centersFL.hide();
              compCentersFL.hide();
            } else {
              centersFL.show();
              compCentersFL.show();
            }
          },
          buttonTitle: function (options, select) {
            var labels = [];
            var pois = [];
            options.each(function () {
              labels.push($(this).text());
              pois.push($(this).data("poi"));
            });

            //appendLocationText(labels, pois);
          },
        });
        $("#locationIDs").multiselect("selectAll", true);
        var uniquecompetitorsCenters = [];
        $.each(competitors, function (i, el) {
          if ($.inArray(el, uniquecompetitorsCenters) === -1)
            uniquecompetitorsCenters.push(el);
        });

        $("#locationIDs").multiselect({
          buttonTitle: function (options, select) {
            var labels = [];
            options.each(function () {
              labels.push($(this).text());
            });
            console.log(labels.join(" - "));
          },
        });

        var previousOptions = null;

        $("#competitorsID").multiselect({
          numberDisplayed: 2,
          includeSelectAllOption: true,
          selectAllValue: "Show All",
          onChange: function (option, checked) {
            // Store the amount of the selected competitors
            var selectedOptions = $("#competitorsID option:selected").length;
            // loop through the currently visible layers
            var visibleListLength = $("#vislayerlist").children().length;
            for (var i = 0; i < visibleListLength; i++) {
              var layerChildId = $("#vislayerlist").children()[i].id;
              // proceed only if the layer is a competitor layer
              if (
                layerChildId === "toggID_49" ||
                layerChildId === "toggID_48" ||
                $("#competitorOverlayChk").prop("checked")
              ) {
                // if the amount of selected options is greater than the amount of previous selected options
                if (
                  selectedOptions >= previousOptions &&
                  // if there is only one competitor left do not send an error
                  !(previousOptions === 0 && selectedOptions === 1) &&
                  // do not send an error on the initial start up
                  !(previousOptions === null)
                ) {
                  swal({
                    title: "Error Encountered",
                    text: "Only one competitor should be selected!",
                    icon: "error",
                  });
                }
              }
            }
            // set the selected options to the previous one
            previousOptions = selectedOptions;
          },
          buttonTitle: function (options, select) {
            var labels = [];
            var ids = [];
            options.each(function () {
              labels.push($(this).text());
              ids.push($(this).context.id);
            });
            //appendCompetitorText(labels, ids);
          },
        });
        $("#competitorsID").multiselect("selectAll", false);
        //Set Layer Definition
        centersFL.setDefinitionExpression("GLOBALID in (" + locationIDs + ")");
        compCentersFL.setDefinitionExpression(
          "GLOBALID in (" + competitorsForDefQUery + ")"
        );
        layerDefinitions[config.centerLayerID] =
          "GLOBALID in (" + locationIDs + ")"; //Definition query for Centers
        layerDefinitions[config.competitorCenterLayerID] =
          "GLOBALID in (" + competitorsForDefQUery + ")"; //Definition query for Competitors
        layerDefinitions[config.competitorShopperOrigins] =
          "GLOBALID in (" + competitorsForDefQUery + ")"; //Definition query for Competitors Trade Areas
        layerDefinitions[config.centerMarketPenetration] =
          "GLOBALID in (" + locationIDs + ")"; //Definition query for Center Penetrations  Areas
        layerDefinitions[config.competitorMarketPenetration] =
          "GLOBALID in (" + competitorsForDefQUery + ")"; //Definition query for Competitors Penetrations  Areas
        layerDefinitions[config.centerRadiusLayerID] =
          "GLOBALID = " + "'" + primaryGlobalID + "'"; //Definition query for Centers
        layerDefinitions[config.geofences] =
          "ab_globalid = " + "'" + primaryGlobalID + "'";
        operationalMapService.setLayerDefinitions(layerDefinitions);
        //Set up list of All Centers and Competitor Centers for later
        allCenters = uniqueArr(locationIDs);
        allCompetitors = uniqueArr(competitorsForDefQUery);
        getCompetorIds();
      })
      .fail(function (jqXHR, textStatus) {
        console.error("Request Failed " + textStatus);
      });
  }

  function uniqueArr(arr) {
    var uniqueArrOfCenters = [];
    $.each(arr, function (i, el) {
      if ($.inArray(el, uniqueArrOfCenters) === -1) uniqueArrOfCenters.push(el);
    });
    return uniqueArrOfCenters;
  }

  var selectedCentersOption;
  var selectedCenter = [],
    selectedCenter2 = [],
    centersForPenetration = [],
    competitorsForPenetration = [],
    centerID = [],
    selectedCentercount = 0;
  $("#locationIDs").on("change", function (e) {
    selectedCentercount = 0;
    first = false;
    //Visitor origin monthly
    $("#centerShopperDiv").hide();
    $("#competitorShopperDiv").hide();
    $("#mktCenterPenDiv").hide();
    $("#mktCompPenDiv").hide();
    $("#centerShopper").button("reset");
    $("#competitorShopper").button("reset");
    $("#centerMktPen").button("reset");
    $("#competitorMktPen").button("reset");
    //Media Coverage
    $("#mediaCoverageDiv").hide();
    mediaLayersCreate.removeMediaCoverageLayers();
    selectedPrimaryIDs = [];
    if (map.graphicsLayerIds.indexOf("pentetrationLayer") != -1)
      map.removeLayer(marketPenetrationFL);
    if (map.graphicsLayerIds.indexOf("compPentetrationLayer") != -1)
      map.removeLayer(compMarketPenetrationFL);
    if (map.graphicsLayerIds.indexOf("originLayer") != -1)
      map.removeLayer(shopperOriginFL);
    if (map.graphicsLayerIds.indexOf("competitorOverlayLayer") != -1)
      map.removeLayer(competitorOverlayFL);
    if (map.graphicsLayerIds.indexOf("zipCodeLayer") != -1)
      map.removeLayer(zipCodeFL);
    layerDefinitions = [];
    centerID = [];
    competitorsForDefQUery = [];
    competitorsForRender = [];
    centersForRenderer = [];
    centersForPenetration = [];
    competitorsForPenetration = [];
    selectedCentersOption = $(this).find("option:selected").text();
    selectedCentersIDOption = $(this).find("option:selected").val();
    selectedCenterPOIID = [];
    primaryGlobalID = [];
    var multiSelect = this.children;
    if (selectedCentersOption == "") {
      $("#advFunctions").hide();
      $("#zipCodeDiv").hide();
      $("#zipCodeDiv").removeClass("layercreated");
      $("#zipCodeChkDiv").hide();
      $("#mktPenDiv").hide();
      $("#mktPenDiv").removeClass("layercreated");
      $("#mktPenChkDiv").hide();
      $("#mktCompPenDiv").hide();
      $("#mktCompPenDiv").removeClass("layercreated");
      $("#mktCompPenChkDiv").hide();
      $("#blkGroupDiv").hide();
      $("#blkGroupDiv").removeClass("layercreated");
      $("#blkGroupChkDiv").hide();
      $("#competitorOverlayDiv").hide();
      $("#competitorOverlayDiv").removeClass("layercreated");
      $("#competitorOverlayChkDiv").hide();
      $("#heatmapDiv").hide();
      $("#heatmapDiv").removeClass("layercreated");
      $("#heatmapChkDiv").hide();
      //Expenditures
      $("#expenditureChkDiv").hide();
      rokTOC.updateLegend("");
      optionsArray = [];
      optionsArray[config.competitorShopperOrigins] = [];
      operationalMapService.setLayerDrawingOptions([]);

      //Set layer definitions for the selected Competitors
      centersFL.setDefinitionExpression("GLOBALID in (" + allCenters + ")");
      compCentersFL.setDefinitionExpression(
        "GLOBALID in (" + allCompetitors + ")"
      );
      layerDefinitions[config.centerLayerID] =
        "GLOBALID in (" + allCenters + ")"; //Definition query for Centers
      layerDefinitions[config.competitorCenterLayerID] =
        "GLOBALID in (" + allCompetitors + ")"; //Definition query for Competitors
      layerDefinitions[config.competitorShopperOrigins] =
        "GLOBALID in (" + allCompetitors + ")"; //Definition query for Competitors Trade Areas
      layerDefinitions[config.centerMarketPenetration] =
        "GLOBALID in (" + locationIDs + ")";
      layerDefinitions[config.competitorMarketPenetration] =
        "GLOBALID in (" + allCompetitors + ")"; //Definition query for Competitors Penetrations  Areas
      layerDefinitions[config.centerRadiusLayerID] =
        "GLOBALID in (" + allCenters + ")"; //Definition query for Centers
      layerDefinitions[config.geofences] = "ab_globalid = ";
      operationalMapService.setLayerDefinitions(layerDefinitions);
      $("#competitorsID").empty();
      $("#competitorsID").multiselect("rebuild");
      $("#competitorsID").multiselect({
        numberDisplayed: 2,
        includeSelectAllOption: true,
        selectAllValue: "Show All",
      });
      $("#competitorsID").multiselect("selectAll", false);
      $("#competitorsID").multiselect("updateButtonText");
      $("#competitorsID").multiselect("disable");
    } else {
      if (masterUser == true) {
        $("#advFunctions").show();
        $("#zipCodeDiv").show();
        $("#zipCodeDiv").removeClass("layercreated");
        $("#zipCodeChkDiv").hide();
        $("#mktPenDiv").show();
        $("#mktPenDiv").removeClass("layercreated");
        $("#mktPenChkDiv").hide();
        $("#mktCompPenDiv").show();
        $("#mktCompPenDiv").removeClass("layercreated");
        $("#mktCompPenChkDiv").hide();
        //Expenditures
        $("#expenditureChkDiv").hide();
      }
      $("#blkGroupDiv").show();
      $("#blkGroupDiv").removeClass("layercreated");
      $("#blkGroupChkDiv").hide();
      $("#competitorOverlayDiv").show();
      $("#competitorOverlayDiv").removeClass("layercreated");
      $("#competitorOverlayChkDiv").hide();
      $("#heatmapDiv").show();
      $("#heatmapDiv").removeClass("layercreated");
      $("#heatmapChkDiv").hide();
      selectedCenter = [];
      selectedCenter2 = []; //$("#locationIDs").val();
      var lsRegExp = /'/g;

      for (var index = 0; index < multiSelect.length; index++) {
        if (multiSelect[index].selected) {
          selectedCentercount = selectedCentercount + 1;
          var centerName = String(multiSelect[index].text).replace(
            lsRegExp,
            "''"
          );
          selectedCenter.push("'" + centerName + "'");
          selectedCenter2.push(centerName);
          selectedPrimaryIDs.push(multiSelect[index].id);
        }
      }
      if (masterUser == true && selectedCentercount == 1) {
        $("#centerShopperDiv").show();
        $("#competitorShopperDiv").show();
        $("#mktCenterPenDiv").show();
        $("#mktCompPenDiv").show();
        //Media Coverage
        $("#mediaCoverageDiv").show();
        $("#mediaCoverageChkDiv").hide();
      }
      $.ajax({
        method: "POST",
        url:
          "https://trutradedevelop.alexanderbabbage.com:5000/maps-develop/filterFromSelectedCenter",
        data: { userId: userID, centerId: selectedPrimaryIDs },
      })
        .done(function (res) {
          $("#competitorsID").empty();
          //Populate Drop down listof available Competitors
          var competitors = [];
          centerID = [];
          for (var i = 0; i < res.length; i++) {
            centerID.push("'" + res[i].PrimaryGlobalID + "'");
            centersForRenderer.push(res[i].PrimaryName);
            centersForPenetration.push(res[i].PrimaryName);
            competitorsForRender.push(
              res[i].CompetitorName.replace(/\s+$/g, "").trim()
            );
            var competitorName = String(res[i].CompetitorName)
              .replace(/'/g, "''")
              .trim();
            competitorsForDefQUery.push("'" + res[i].CompetitorGlobalID + "'");
            compCenters.push("'" + res[i].CompetitorGlobalID + "'");
            competitors.push(
              "<option name='" +
                res[i].CompetitorName +
                "' id='" +
                res[i].CompetitorGlobalID +
                "' value='" +
                res[i].CompetitorGlobalID +
                "'>" +
                res[i].CompetitorName +
                "</option>"
            );
            competitorsForPenetration.push("'" + competitorName.trim() + "'");
          }

          var uniquecompetitorsCenters = [];
          $.each(competitors, function (i, el) {
            if ($.inArray(el, uniquecompetitorsCenters) === -1)
              uniquecompetitorsCenters.push(el);
          });
          $("#competitorsID").append(uniquecompetitorsCenters.sort());
          $("#competitorsID").multiselect("rebuild");
          $("#competitorsID").multiselect({
            numberDisplayed: 2,
            includeSelectAllOption: true,
            selectAllValue: "Show All",
          });
          $("#competitorsID").multiselect("selectAll", false);
          $("#competitorsID").multiselect("updateButtonText");
          $("#competitorsID").multiselect("enable");
          //Set Layer Definition
          getMaxDeviceVal(
            selectedCenter,
            competitorsForDefQUery,
            competitorsForRender,
            selectedPrimaryIDs
          );
          layerDefinitions[config.geofences] =
            "ab_globalid = " + "'" + selectedPrimaryIDs + "'";
          zoomToExtentofCompetitors(selectedPrimaryIDs);
          //UPDATE LEGEND
          setTimeout(function () {
            rokTOC.updateLegend($("#legendLayers").val());
          }, 3000);
        })
        .fail(function (jqXHR, textStatus) {
          console.error("Request Failed " + textStatus);
        });
    }
  });
  var compCenters = [];
  $("#competitorsID").on("change", function (e) {
    var selectedOption = $(this).find("option:selected").text();
    var multiSelect = this.children;
    if (map.graphicsLayerIds.indexOf("compPentetrationLayer") != -1)
      map.removeLayer(compMarketPenetrationFL);
    if (selectedOption == "") {
      if (masterUser == true) {
        $("#competitorShopper").removeClass("btn-info");
        $("#competitorShopper").addClass("btn-primary");
        $("#competitorMktPen").removeClass("btn-info");
        $("#competitorMktPen").addClass("btn-primary");
        $("#mktCompPenDiv").show();
        $("#mktCompPenChkDiv").hide();
        $("#mktCompPenDiv").removeClass("layercreated");
      }
      $("#zipCodeChkDiv").hide();
      $("#zipCodeDiv").removeClass("layercreated");
      $("#zipCodeDiv").show();
      $("#blkGroupChkDiv").hide();
      $("#blkGroupDiv").removeClass("layercreated");
      $("#blkGroupDiv").show();
      $("#competitorOverlayDiv").show();
      $("#competitorOverlayDiv").removeClass("layercreated");
      $("#competitorOverlayChkDiv").hide();
      $("#heatmapDiv").show();
      $("#heatmapDiv").removeClass("layercreated");
      $("#heatmapChkDiv").hide();
      (compCenters = []), (competitorsForPenetration = []);
      //Set layer definitions for the selected Competitors
      compCentersFL.setDefinitionExpression("GLOBALID in ('')");
      layerDefinitions[config.competitorCenterLayerID] = "GLOBALID in ('')"; //Definition query for Competitors
      layerDefinitions[config.competitorShopperOrigins] = "GLOBALID in ('')"; //Definition query for Competitors Trade Areas
      operationalMapService.setLayerDefinitions(layerDefinitions);
      changeLayerRenderers(selectedCenter, []);
    } else {
      if (masterUser == true) {
        $("#competitorShopper").removeClass("btn-info");
        $("#competitorShopper").addClass("btn-primary");
        $("#competitorMktPen").removeClass("btn-info");
        $("#competitorMktPen").addClass("btn-primary");
        $("#mktCompPenDiv").show();
        $("#mktCompPenDiv").removeClass("layercreated");
        $("#mktCompPenChkDiv").hide();
      }
      $("#zipCodeChkDiv").hide();
      $("#zipCodeDiv").removeClass("layercreated");
      $("#zipCodeDiv").show();
      $("#blkGroupChkDiv").hide();
      $("#blkGroupDiv").removeClass("layercreated");
      $("#blkGroupDiv").show();
      $("#competitorOverlayDiv").show();
      $("#competitorOverlayDiv").removeClass("layercreated");
      $("#competitorOverlayChkDiv").hide();
      $("#heatmapDiv").show();
      $("#heatmapDiv").removeClass("layercreated");
      $("#heatmapChkDiv").hide();
      //Set layer definitions for the selected Competitors
      (competitorsForRender = []),
        (compCenters = []),
        (compsforRenderer = []),
        (competitorGlobalIDs = []),
        (competitorsForPenetration = []);
      var lsRegExp = /'/g;
      for (var index = 0; index < multiSelect.length; index++) {
        if (multiSelect[index].selected) {
          var competitorName = String(multiSelect[index].text).replace(
            lsRegExp,
            "''"
          );
          compCenters.push("'" + multiSelect[index].value + "'");
          competitorsForRender.push(competitorName);
          competitorsForPenetration.push("'" + competitorName + "'");
          competitorGlobalIDs.push("'" + multiSelect[index].value + "'");
        }
      }
      compCentersFL.setDefinitionExpression(
        "GLOBALID in (" + competitorGlobalIDs + ")"
      );
      layerDefinitions[config.competitorCenterLayerID] =
        "GLOBALID in (" + competitorGlobalIDs + ")";
      layerDefinitions[config.competitorShopperOrigins] =
        "GLOBALID in (" + competitorGlobalIDs + ")";
      layerDefinitions[config.competitorMarketPenetration] =
        "GLOBALID in (" + competitorGlobalIDs + ")"; //Definition query for Competitors Penetrations  Areas
      operationalMapService.setLayerDefinitions(layerDefinitions);
      changeLayerRenderers(selectedCenter, competitorGlobalIDs);
    }
  });

  function getMaxDeviceVal(
    centers,
    competitors,
    competitorsForRender,
    selectedPrimaryIDs
  ) {
    selectedPrimaryIDs = "'" + selectedPrimaryIDs + "'";
    if (heatmapFeatureLayer)
      heatmapFeatureLayer.setDefinitionExpression(
        "GLOBALID = " + "'" + primaryGlobalID + "'"
      );
    centersFL.setDefinitionExpression(
      "GLOBALID in (" + selectedPrimaryIDs + ")"
    );
    compCentersFL.setDefinitionExpression("GLOBALID in (" + competitors + ")");
    layerDefinitions[config.centerLayerID] =
      "GLOBALID in (" + selectedPrimaryIDs + ")"; //Definition query for Centers
    layerDefinitions[config.competitorCenterLayerID] =
      "GLOBALID in (" + competitors + ")"; //Definition query for Competitors
    layerDefinitions[config.centerShopperOrigins] =
      "GLOBALID in (" + selectedPrimaryIDs + ")";
    layerDefinitions[config.competitorShopperOrigins] =
      "GLOBALID in (" + competitors + ")"; //AND TotalDevices = '"+result.features[0].attributes.maxDevices+"'";  //Definition query for Competitors Trade Areas
    layerDefinitions[config.centerMarketPenetration] =
      "GLOBALID in (" + selectedPrimaryIDs + ")"; //Definition query for Center Penetrations  Areas
    layerDefinitions[config.centerRadiusLayerID] =
      "GLOBALID in (" + selectedPrimaryIDs + ")"; //Definition query for Centers
    operationalMapService.setLayerDefinitions(layerDefinitions);
    changeLayerRenderers(centers, competitors);
    if (first == false) {
      setTimeout(function (e) {
        // mediaLayersCreate.createTVRadioLayers(map, selectedPrimaryIDs, 0);
      }, 3000);
    }
  }

  function changeLayerRenderers(center, comps) {
    /************* Competitor Visitor Origins **************/
    var compTradeAreaDrawingOption = new LayerDrawingOptions();
    var compTradeAreaRenderer = new UniqueValueRenderer(null, "CompetitorName");

    var compVal = "";
    if (comps.length != 0) {
      compVal = comps[0].replace("''", "'");
    }
    var redColor = Color.fromHex(compTadeAreaColorList[0]);
    fillSymbolC.setColor(new Color(redColor));
    fillSymbolC.setStyle("solid");
    symbolC = fillSymbolC;
    compTradeAreaRenderer.addValue({
      value: compVal,
      label: compVal,
      description: null,
      symbol: new SimpleFillSymbol()
        .setColor(new Color(redColor))
        .setOutline(new SimpleLineSymbol().setWidth(0)),
    });

    compTradeAreaDrawingOption.renderer = compTradeAreaRenderer;
  }
  $("#mediaCoverageBtn").on("click", function (e) {
    if ($("#mediaCoverageChkDiv").is(":visible")) {
      $("#mediaCoverageChkDiv").hide();
    } else if (
      $("#mediaCoverageChkDiv").hasClass("layercreated") &&
      !$("#mediaCoverageChkDiv").is(":visible")
    ) {
      $("#mediaCoverageChkDiv").show();
    }
    if (!$("#mediaCoverageChkDiv").hasClass("layercreated")) {
      $("#mediaCoverageBtn").removeClass("btn-primary");
      $("#mediaCoverageBtn").addClass("btn-info");
      // mediaLayersCreate.createTVRadioLayers(map, selectedCenterID, 0);
      $("#mediaCoverageChkDiv").addClass("layercreated");
    }
  });
  $("#getPopulationBtn").on("click", function (e) {
    mediaLayersCreate.getShopperPopWithInTV();
  });
  //CenterShopper Origins Time Options
  $("#centerShopper").on("click", function () {
    if ($("#bottomPanel").is(":visible")) {
      $("#" + config.centerShopperOrigins).attr("disabled", false);
    } else {
      $("#" + config.competitorShopperOrigins).attr("disabled", false);
      $("#" + config.centerShopperOrigins).attr("disabled", true);
      if ($("#" + config.centerShopperOrigins).is(":checked")) {
        $("#" + config.centerShopperOrigins).attr("checked", false);
        var vislayers = operationalMapService.visibleLayers;
        vislayers = removeA(vislayers, config.centerShopperOrigins);
        operationalMapService.setVisibleLayers(vislayers);
      }
    }
  });
  //CompetitorShopper Origins Time Options
  $("#competitorShopper").on("click", function () {
    if ($("#competitorBottomPanel").is(":visible")) {
      $("#" + config.competitorShopperOrigins).attr("disabled", false);
    } else {
      $("#" + config.centerShopperOrigins).attr("disabled", false);
      $("#" + config.competitorShopperOrigins).attr("disabled", true);
      if ($("#" + config.competitorShopperOrigins).is(":checked")) {
        $("#" + config.competitorShopperOrigins).attr("checked", false);
        var vislayers = operationalMapService.visibleLayers;
        vislayers = removeA(vislayers, config.competitorShopperOrigins);
        operationalMapService.setVisibleLayers(vislayers);
      }
    }
  });
  //Center Penetration Time Layers
  $("#centerMktPen").on("click", function () {
    if ($("#bottomPanel").is(":visible")) {
      $("#" + config.centerMarketPenetration).attr("disabled", false);
    } else {
      $("#" + config.competitorMarketPenetration).attr("disabled", false);
      $("#" + config.centerMarketPenetration).attr("disabled", true);
      if ($("#" + config.centerMarketPenetration).is(":checked")) {
        $("#" + config.centerMarketPenetration).attr("checked", false);
        var vislayers = operationalMapService.visibleLayers;
        vislayers = removeA(vislayers, config.centerMarketPenetration);
        operationalMapService.setVisibleLayers(vislayers);
      }
    }
  });
  //CompetitorShopper Origins Time Options
  $("#competitorMktPen").on("click", function () {
    if ($("#competitorBottomPanel").is(":visible")) {
      $("#" + config.competitorMarketPenetration).attr("disabled", false);
    } else {
      $("#" + config.centerMarketPenetration).attr("disabled", false);
      $("#" + config.competitorMarketPenetration).attr("disabled", true);
      if ($("#" + config.competitorMarketPenetration).is(":checked")) {
        $("#" + config.competitorMarketPenetration).attr("checked", false);
        var vislayers = operationalMapService.visibleLayers;
        vislayers = removeA(vislayers, config.competitorMarketPenetration);
        operationalMapService.setVisibleLayers(vislayers);
      }
    }
  });

  function removeA(arr) {
    var what,
      a = arguments,
      L = a.length,
      ax;
    while (L > 1 && arr.length) {
      what = a[--L];
      while ((ax = arr.indexOf(what)) !== -1) {
        arr.splice(ax, 1);
      }
    }
    return arr;
  }
  //Street View and Bing Birds Eye
  var viewerTool;
  $("#birdsEyebtn").on("click", function () {
    viewerTool = "bing";
    $("#viewType").html("Bing");
    deActivateTools("bingView");
    $(".btn").removeClass("active");
    $(this).addClass("mapBtnsActive").siblings().removeClass("mapBtnsActive");
    map.setInfoWindowOnClick(false); //Deactivate Infowindow
    birdsEyeView = map.on("click", openStreetViewWindow);
  });
  function openStreetViewWindow(evt) {
    var params = new ProjectParameters();
    params.geometries = [evt.mapPoint];
    params.inSR = new SpatialReference(4326); //{wkid: 102719};
    params.outSR = new SpatialReference(4326); //{wkid: 4326};
    gsvc.project(params);
  }
  function configureMapClickCoords(evtObj) {
    if (isCoordsTool == true) {
      completeCoordsMarker(evtObj);
      isCoordsTool = false;
    } else if (isCoordsZoom == true) {
      zoomToCoords(evtObj);
      isCoordsZoom = false;
    } else {
      $(".btn").removeClass("mapBtnsActive");
      $("#gstrtview").empty();
      $("#gstrtviewMobile").empty();
      if (viewerTool == "bing") {
        birdsEyeView.remove();
        $("#gstrtviewMobile").attr(
          "src",
          "html/bing.html?lat=" +
            evtObj.geometries[0].y.toFixed(6) +
            "&lon=" +
            evtObj.geometries[0].x.toFixed(6) +
            ""
        );
        $("#googleStreetviewDiv").modal("show");
      }
      viewerTool = "";
      map.setInfoWindowOnClick(true); //Reactivate infowindow
      $("#panbtn").addClass("active");
    }
  }
  //Get device width and height
  function update_innerWidth() {
    var modalW = window.innerWidth - 50 + "px";
    $("#gstrtviewMobile").css("width", modalW);
  }

  update_innerWidth();
  //Update with on resize and orientation
  window.addEventListener("resize", update_innerWidth, false);
  window.addEventListener("orientationchange", update_innerWidth, false);

  //End Street View
  function deActivateTools(activeTool) {
    switch (activeTool) {
      case "navTools":
        if (birdsEyeView) birdsEyeView.remove();
        redLineTextTB.deactivate();
        measureAreatb.deactivate();
        measureDistancetb.deactivate();
        coordstb.deactivate();
        redLineTextTB.deactivate();
        redLineTB.deactivate();
        break;
      case "selectTools":
        if (birdsEyeView) birdsEyeView.remove();
        redLineTextTB.deactivate();
        redLineTB.deactivate();
        measureAreatb.deactivate();
        measureDistancetb.deactivate();
        coordstb.deactivate();
        navToolbar.deactivate();
        if (zoomInHandler) zoomInHandler.remove();
        if (zoomOutHandler) zoomOutHandler.remove();
        break;
      case "deSelectTools":
        if (birdsEyeView) birdsEyeView.remove();
        redLineTextTB.deactivate();
        redLineTB.deactivate();
        measureAreatb.deactivate();
        measureDistancetb.deactivate();
        coordstb.deactivate();
        navToolbar.deactivate();
        if (zoomInHandler) zoomInHandler.remove();
        if (zoomOutHandler) zoomOutHandler.remove();
        break;
      case "redLineTools":
        if (birdsEyeView) birdsEyeView.remove();
        redLineTextTB.deactivate();
        measureAreatb.deactivate();
        measureDistancetb.deactivate();
        coordstb.deactivate();
        navToolbar.deactivate();
        if (zoomInHandler) zoomInHandler.remove();
        if (zoomOutHandler) zoomOutHandler.remove();
        break;
      case "redLineTextTools":
        if (birdsEyeView) birdsEyeView.remove();
        redLineTB.deactivate();
        if (measurementWidget) measurementWidget.setTool("area", false);
        measureAreatb.deactivate();
        measureDistancetb.deactivate();
        if (measurementWidget) measurementWidget.setTool("distance", false);
        if (measurementWidget) measurementWidget.setTool("location", false);
        coordstb.deactivate();
        navToolbar.deactivate();
        if (zoomInHandler) zoomInHandler.remove();
        if (zoomOutHandler) zoomOutHandler.remove();
        break;
      case "measureAreaTool":
        if (birdsEyeView) birdsEyeView.remove();
        redLineTextTB.deactivate();
        redLineTB.deactivate();
        coordstb.deactivate();
        navToolbar.deactivate();
        if (zoomInHandler) zoomInHandler.remove();
        if (zoomOutHandler) zoomOutHandler.remove();
        break;
      case "measureDistanceTool":
        if (birdsEyeView) birdsEyeView.remove();
        redLineTextTB.deactivate();
        redLineTB.deactivate();
        coordstb.deactivate();
        navToolbar.deactivate();
        if (zoomInHandler) zoomInHandler.remove();
        if (zoomOutHandler) zoomOutHandler.remove();
        break;
      case "coords":
        if (birdsEyeView) birdsEyeView.remove();
        redLineTextTB.deactivate();
        redLineTB.deactivate();
        measureAreatb.deactivate();
        measureDistancetb.deactivate();
        navToolbar.deactivate();
        if (zoomInHandler) zoomInHandler.remove();
        if (zoomOutHandler) zoomOutHandler.remove();
        break;
      case "streetView":
        if (birdsEyeView) birdsEyeView.remove();
        break;
      case "bingView":
        break;
    }
  }
  //HEAT MAP DIV
  $("#heatmapBtn").on("click", function (e) {
    if ($("#heatmapChkDiv").is(":hidden")) {
      $("#heatmapChkDiv").show();
    } else {
      $("#heatmapChkDiv").hide();
    }
  });
  $("#heatmapChk").on("click", function (e) {
    if ($(".blurInfo").is(":hidden")) {
      heatmapFeatureLayer.setDefinitionExpression(
        "GLOBALID = " + "'" + primaryGlobalID + "'"
      );
      $(".blurInfo").show();
      heatmapFeatureLayer.show();
    } else {
      $(".blurInfo").hide();
      heatmapFeatureLayer.hide();
    }
  });
  ///Dynamically Competitor Overlay Maps
  $("#competitorOverlayButton").on("click", function (e) {
    if ($("#competitorOverlayChkDiv").is(":visible")) {
      $("#competitorOverlayChkDiv").hide();
    } else if (
      $("#competitorOverlayDiv").hasClass("layercreated") &&
      !$("#competitorOverlayChkDiv").is(":visible")
    ) {
      $("#competitorOverlayChkDiv").show();
    }
    if (!$("#competitorOverlayDiv").hasClass("layercreated")) {
      $("#competitorOverlayChk").attr("checked", false);
      var uniqueCenters = [];
      $.each(centerID, function (i, el) {
        if ($.inArray(el, uniqueCenters) === -1) uniqueCenters.push(el);
      });
      $.ajax({
        method: "GET",
        url:
          "../penetration/penetration.asmx/GetOverlay?sCenter1=" +
          uniqueCenters[0] +
          "&sCenter2=" +
          compCenters[0] +
          "",
      }).done(function (res) {
        //Send off the temp table name to the create the penetration layer
        if (map.graphicsLayerIds.indexOf("competitorOverlayLayer") != -1)
          map.removeLayer(competitorOverlayFL);
        var tablename = res; //competitorsForRender
        tablename = $(tablename).find("string").text();
        createCompetitorOverlayLayer.createCompetitorOverlayLayer(
          map,
          operationalMapService,
          tablename.replace(/"/g, ""),
          competitorsForRender,
          centersForRenderer,
          compTadeAreaColorList
        ); // "TempBlkGrpDevices"
        $("#competitorOverlayDiv").addClass("layercreated");
      });
    }
  });
  $("#competitorOverlayChk").on("click", function (e) {
    if ($("#competitorOverlayChk").is(":checked")) {
      competitorOverlayFL.show();
    } else {
      competitorOverlayFL.hide();
    }
  });
  // Zip Code Winner
  $("#zipCodeBtn").on("click", function (e) {
    if ($("#zipCodeChkDiv").is(":visible")) {
      $("#zipCodeChkDiv").hide();
    } else if (
      $("#zipCodeDiv").hasClass("layercreated") &&
      !$("#zipCodeChkDiv").is(":visible")
    ) {
      $("#zipCodeChkDiv").show();
    }
    if (!$("#zipCodeDiv").hasClass("layercreated")) {
      $("#zipCodeChk").attr("checked", false);
      var uniqueCenters = [];
      $.each(centerID, function (i, el) {
        if ($.inArray(el, uniqueCenters) === -1) {
          uniqueCenters.push(el);
        }
      });
      $.ajax({
        method: "GET",
        url:
          "../penetration/penetration.asmx/GetHighestZip?sCenter=(" +
          uniqueCenters +
          "," +
          compCenters +
          ")",
      }).done(function (res) {
        //Send off the temp table name to the create the penetration layer
        if (map.graphicsLayerIds.indexOf("zipCodeLayer") != -1)
          map.removeLayer(zipCodeFL);
        var tablename = res.firstChild.textContent; //competitorsForRender
        zipCodeLayer.createZipCodeLayer(
          map,
          operationalMapService,
          tablename.replace(/"/g, ""),
          competitorsForRender,
          centersForRenderer,
          compTadeAreaColorList
        ); // "TempBlkGrpDevices"
        $("#zipCodeDiv").addClass("layercreated");
      });
    }
  });

  $("#zipCodeChk").on("click", function (e) {
    if ($("#zipCodeChk").is(":checked")) {
      zipCodeFL.show();
    } else {
      zipCodeFL.hide();
    }
  });

  ///Center Market Penetration Layer
  $("#mktPenBtn").on("click", function (e) {
    if ($("#mktPenChkDiv").is(":visible")) {
      $("#mktPenChkDiv").hide();
    } else if (
      $("#mktPenDiv").hasClass("layercreated") &&
      !$("#mktPenChkDiv").is(":visible")
    ) {
      $("#mktPenChkDiv").show();
    }
    if (!$("#mktPenDiv").hasClass("layercreated")) {
      $("#mktPen").attr("checked", false);
      $.post(
        "../penetration/penetration.asmx/GetPenetrationData?",
        { sCenter: "(" + centersForPenetration + ")" },
        function (res) {
          //Send off the temp table name to the create the penetration layer
          if (map.graphicsLayerIds.indexOf("pentetrationLayer") != -1)
            map.removeLayer(marketPenetrationFL);
          var tablename = res.firstChild.textContent;
          CenterDynamicPenetrationLayer.createPenetrationLayer(
            map,
            operationalMapService,
            tablename.replace(/"/g, "")
          ); // "TempBlkGrpDevices"
          $("#mktPenDiv").addClass("layercreated");
        }
      );
    }
  });

  ///Competitor Dynamic Penetration Layer
  $("#mktCompPenBtn").on("click", function (e) {
    if ($("#mktCompPenChkDiv").is(":visible")) {
      $("#mktCompPenChkDiv").hide();
    } else if (
      $("#mktCompPenDiv").hasClass("layercreated") &&
      !$("#mktCompPenChkDiv").is(":visible")
    ) {
      $("#mktCompPenChkDiv").show();
    }
    if (!$("#mktCompPenDiv").hasClass("layercreated")) {
      var $btn = $(this).button("loading");
      $("#mktCompPen").attr("checked", false);
      $.post(
        "../penetration/penetration.asmx/GetPenetrationData?",
        { sCenter: "(" + competitorsForPenetration + ")" },
        function (res) {
          //Send off the temp table name to the create the penetration layer
          if (map.graphicsLayerIds.indexOf("compPentetrationLayer") != -1)
            map.removeLayer(compMarketPenetrationFL);

          var tablename = res.firstChild.textContent;
          CompetitorDynamicPenetrationLayer.createPenetrationLayer(
            map,
            operationalMapService,
            tablename.replace(/"/g, "")
          ); // "TempBlkGrpDevices"
          $("#mktCompPenDiv").addClass("layercreated");
        }
      );
    }
  });
  ///Dynamically Visitor Origin Maps
  $("#blkGroupButton").on("click", function (e) {
    if ($("#blkGroupChkDiv").is(":visible")) {
      $("#blkGroupChkDiv").hide();
    } else if (
      $("#blkGroupDiv").hasClass("layercreated") &&
      !$("#blkGroupChkDiv").is(":visible")
    ) {
      $("#blkGroupChkDiv").show();
    }
    if (!$("#blkGroupDiv").hasClass("layercreated")) {
      $("#shopperOrigin").attr("checked", false);
      var uniqueCenters = [];
      $.each(centerID, function (i, el) {
        if ($.inArray(el, uniqueCenters) === -1) {
          uniqueCenters.push(el);
        }
      });
      $.ajax({
        method: "GET",
        url:
          "../penetration/penetration.asmx/GetHighest?sCenter=(" +
          uniqueCenters +
          "," +
          compCenters +
          ")",
      }).done(function (res) {
        //Send off the temp table name to the create the penetration layer
        if (map.graphicsLayerIds.indexOf("originLayer") != -1)
          map.removeLayer(shopperOriginFL);
        var tablename = res.firstChild.textContent; //competitorsForRender
        CenterDynamicShopperOriginLayer.createShopperOriginLayer(
          map,
          operationalMapService,
          tablename.replace(/"/g, ""),
          competitorsForRender,
          centersForRenderer,
          compTadeAreaColorList
        ); // "TempBlkGrpDevices"
        $("#blkGroupDiv").addClass("layercreated");
      });
    }
  });

  $("#shopperOrigin").on("click", function (e) {
    if ($("#shopperOrigin").is(":checked")) {
      shopperOriginFL.show();
    } else {
      shopperOriginFL.hide();
    }
  });

  //Print Map
  $(".closeprintbtn").on("click", function () {
    $("#printDiv").toggle();
  });
  $("#printbtn").click(function (e) {
    $("#printDiv").toggle();
  });
  $("#btnPrint").click(function (e) {
    var legendLayer = new esri.tasks.LegendLayer();
    legendLayer.layerId = "operationalMapService";
    legendLayer.subLayerIds = [];
    //Set up print stuff
    $("#loading").show();
    var printTask = new PrintTask(config.printTaskURL);
    var params = new PrintParameters();
    var template = new PrintTemplate();

    params.map = map;
    template.exportOptions = {
      width: 595,
      height: 842,
      dpi: 96,
    };
    template.format = $("#printformat").val();
    template.layout = $("#printlayout").val();
    template.preserveScale = true;
    template.layoutOptions = {
      titleText: $("#printtitle").val(),
      copyrightText: new Date().getFullYear(),
      scalebarUnit: "Feet",
    };
    params.template = template;
    printTask.execute(params, printResult, printError);
    setTimeout(function () {
      $("#printDiv").toggle();
    }, 1000);
  });
  function printResult(result) {
    var count = result.url.lastIndexOf("/");
    var newURL = result.url.slice(count + 1);
    var theurl =
      "https://trutradedevelop.alexanderbabbage.com/output/" + newURL;
    window.open(result.url);
    // var printType = result.url.slice(result.url.length - 3);
    // $.ajax({
    //   method: "POST",
    //   url:
    //     "https://trutradedevelop.alexanderbabbage.com:5000/printing/getPrintPdf",
    //   data: { url: result.url + "", printType: printType },
    // }).then((data) => {
    //   var pdfWindow = window.open("", "");
    //   pdfWindow.document.write(
    //     "<iframe width='100%' height='100%' src='data:application/pdf;base64, " +
    //       encodeURI(data) +
    //       "'></iframe>"
    //   );
    // });
  }
  function printError(result) {
    $("#loading").hide();
  }
  //End Printing
  // function openExportModal() {
  //   $(".dialog").dialog({
  //     autoOpen: false,
  //   });
  //   $(".sf-dialog-btn").click(function () {
  //     $(".dialog").dialog("open");
  //   });
  // }
  // openExportModal();

  // var prevLength = 0;
  // var locationsList = [];
  // var competitorsList = [];
  // var locationsPois = [];
  // var competitorIds = [];

  // function appendLocationText(locations, pois) {
  //   if (locations.length === 0) {
  //     $(".selectedCenters li").empty();
  //     $(".selectedCompetitors li").empty();
  //     return;
  //   }

  //   if (locations.length !== prevLength) {
  //     $(".selectedCenters li").empty();
  //     for (var i = 0; i < locations.length; i++) {
  //       var selectedCenterText = $(
  //         "<li class='selected-text'>" + locations[i] + "</li>"
  //       );
  //       $(".selectedCenters").append(selectedCenterText);
  //     }
  //     locationsList = locations;
  //     locationsPois = pois;
  //   }
  // }

  // function appendCompetitorText(competitors, ids) {
  //   if (competitors.length === 0) $(".selectedCompetitors li").empty();
  //   if (competitors.length !== prevLength) {
  //     $(".selectedCompetitors li").empty();
  //     for (var i = 0; i < competitors.length; i++) {
  //       var selectedCompetitorText = $(
  //         "<li class='selected-text'>" + " " + competitors[i] + "</li>"
  //       );
  //       $(".selectedCompetitors").append(selectedCompetitorText);
  //     }
  //     competitorsList = competitors;
  //     competitorIds = ids;
  //   }
  // }

  // $(".ui-icon-closethick").text("X");

  // $(".sf-dialog-btn").on();

  // $("form[name='shapeFilesForm']")
  //   .submit(function (e) {
  //     e.preventDefault();
  //   })
  //   .validate({
  //     rules: {
  //       email: {
  //         required: true,
  //         email: true,
  //       },
  //     },
  //     messages: {
  //       email: "Please enter a valid email address",
  //     },
  //     submitHandler: function (form) {
  //       $.ajax({
  //         method: "POST",
  //         url:
  //           "https://trutradedevelop.alexanderbabbage.com:5000/maps/postShapeFilesData",
  //         data: {
  //           locations: locationsList,
  //           competitors: competitorsList,
  //           locationPois: locationsPois,
  //           competitorIds: competitorIds,
  //           email: $(".sf-form-email").val(),
  //         },
  //       }).done(function () {
  //         $(".form-submitted").css("visibility", "visible");
  //         $(".form-submitted").css("display", "block");
  //       });
  //     },
  //   });

  $(".buttonTT").on("click", function () {
    var btn = this;
    var originalText = "" + $(btn).text();
    var dataLoadingText = $(btn).attr("data-loading-text");
    var parentDiv = $(btn).parent().attr("id");
    if (
      parentDiv === "blkGroupDiv" ||
      parentDiv === "competitorOverlayDiv" ||
      parentDiv === "zipCodeDiv"
    ) {
      parentDiv.slice(0, -3);
      $(btn).text(dataLoadingText);
      var targetNode = $(".optionsDiv.advancedLayersDiv")[0];
      var config = { attributes: true, subtree: true };
      var callback = function (mutationList, observer) {
        var lengthOfList = mutationList.length;
        for (var i = 0; i < lengthOfList; i++) {
          if (mutationList[i].attributeName === "style") {
            $(btn).text(originalText);
            observer.disconnect();
          }
        }
      };
      var observer = new MutationObserver(callback);
      observer.observe(targetNode, config);
    }
  });
});
