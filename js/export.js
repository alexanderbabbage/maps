function exportSelections(exportData){
	var headers = [], newLayerIDs = [], rowsArrayCollection=[], layername = "";
	//"<table>";
	//Get Distinct layerids
	$.each(exportData, function(i, item){
		if ($.inArray(item.layerId,newLayerIDs) === -1){
			newLayerIDs.push(item.layerId);
		}
	});

	for(layerid in newLayerIDs){
		//console.log(newLayerIDs[layerid])
		headers = [], rowsArrayCollection= [], layername = "";
		$.each(exportData,function(clientIdx,item){
			if(item.layerId == newLayerIDs[layerid]){
				layername = item.layerName;
				for(rowAttributeIndex in item.feature.attributes)
				{
					if ($.inArray(rowAttributeIndex,headers) === -1){
						headers.push(rowAttributeIndex);
					}
				}
			}
		});
		var html = "";
		html +="<tr>";
		$.each(headers,function(clientIdx,item)
		{
			html +="<th>"+item+"</th>";

		});
		html +="</tr>";
		for(resultFeatureIndex in exportData)
		{
			if(exportData[resultFeatureIndex].layerId == newLayerIDs[layerid]){
				//set up a temporary array that will store attributes for each feature
				//and it will be reset for each feature
				var tempRow = [];
				//Then loop through the feature's attributes and push them into temp array
				for(rowAttributeIndex in exportData[resultFeatureIndex].feature.attributes)
				{
					tempRow.push(exportData[resultFeatureIndex].feature.attributes[rowAttributeIndex]);
				}
				//push feature's attribute array into Array Collection
				rowsArrayCollection.push(tempRow);
			}
		}


		for (var fieldName in rowsArrayCollection){
			html +="<tr>";
			html+='<td>'+rowsArrayCollection[fieldName].join('</td><td>')+'</td>';
			html +="</tr>";
		}
		$('<table>').attr({'id':'excelTable', 'name': 'excelTable'}).appendTo('#dvData');
		$("#excelTable").append(html);
		exportTableToCSV.apply(this, [$('#dvData>table'), ''+layername+'.csv']);
		$("#excelTable").empty();
	}
}

function exportDetailsResults(results, layername){

	var headers = [],rowsArrayCollection=[];
	var html = "";//"<table>";
	html +="<tr>";
	for (var fieldAliase in results.fieldAliases){
		html +="<th>"+results.fieldAliases[fieldAliase]+"</th>";
	}
	html +="</tr>";

	for(resultFeatureIndex in results.features)
	{
		//set up a temporary array that will store attributes for each feature
		//and it will be reset for each feature
		var tempRow = [];
		//Then loop through the feature's attributes and push them into temp array
		for(rowAttributeIndex in results.features[resultFeatureIndex].attributes)
		{
			tempRow.push(results.features[resultFeatureIndex].attributes[rowAttributeIndex]);
		}
		//push feature's attribute array into Array Collection
		rowsArrayCollection.push(tempRow);
	}
	for (var fieldName in rowsArrayCollection){
		html +="<tr>";
		html+='<td>'+rowsArrayCollection[fieldName].join('</td><td>')+'</td>';
		html +="</tr>";
	}
	$('<table>').attr({'id':'excelTable', 'name': 'excelTable'}).appendTo('#dvData');
	$("#excelTable").append(html);
	exportTableToCSV.apply(this, [$('#dvData>table'), ''+layername+'.csv']);
	$("#excelTable").empty();
}

function exportTableToCSV($table, filename) {
	var $headers = $table.find('tr:has(th)')
	,$rows = $table.find('tr:has(td)')

	// Temporary delimiter characters unlikely to be typed by keyboard
	// This is to avoid accidentally splitting the actual contents
	,tmpColDelim = String.fromCharCode(11) // vertical tab character
	,tmpRowDelim = String.fromCharCode(0) // null character

	// actual delimiter characters for CSV format
	,colDelim = '","'
	,rowDelim = '"\r\n"';

	// Grab text from table into CSV formatted string
	var csv = '"';
	csv += formatRows($headers.map(grabRow));
	csv += rowDelim;
	csv += formatRows($rows.map(grabRow)) + '"';

	// Data URI
	//console.log(csv);
	var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);
	if (isIE || IE.isTheBrowser) {
		csvData = csv//decodeURIComponent(csv);

		var iframe = document.getElementById('csvDownloadFrame');
		iframe = iframe.contentWindow || iframe.contentDocument;

		csvData = 'sep=,\r\n' + csvData;

		iframe.document.open("text/html", "replace");
		iframe.document.write(csvData);
		iframe.document.close();
		iframe.focus();
		iframe.document.execCommand('SaveAs', true, filename);
		$("#excelloading").hide();
		$("#exportToExcelbtn").show();
	} else {
		//location.href = csvData;
		/* $(this)
		.attr({
			'download': filename,
			'href': csvData,
			'target': '_blank'
		});*/
		//  $(this).click();
		var fn = filename || "download",
		D = document,
		a = D.createElement("a");
		a.href = csvData;
		a.setAttribute("download", fn);
		a.innerHTML = "downloading...";
		D.body.appendChild(a);
		setTimeout(function() {
			a.click();
			D.body.removeChild(a);
			//if(winMode===true){setTimeout(function(){ self.URL.revokeObjectURL(a.href);}, 250 );}
			$("#excelloading").hide();
			$("#exportToExcelbtn").show();
		}, 66);
		return true;
	}
	//}

	//------------------------------------------------------------
	// Helper Functions
	//------------------------------------------------------------
	// Format the output so it has the appropriate delimiters
	function formatRows(rows){
		return rows.get().join(tmpRowDelim)
		.split(tmpRowDelim).join(rowDelim)
		.split(tmpColDelim).join(colDelim);
	}
	// Grab and format a row from the table
	function grabRow(i,row){

		var $row = $(row);
		//for some reason $cols = $row.find('td') || $row.find('th') won't work...
		var $cols = $row.find('td');
		if(!$cols.length) $cols = $row.find('th');

		return $cols.map(grabCol)
		.get().join(tmpColDelim);
	}
	// Grab and format a column from the table
	function grabCol(j,col){
		var $col = $(col),
		$text = $col.text();

		return $text.replace('"', '""'); // escape double quotes
	}
}

var UAString = navigator.userAgent;
var isIE = false;
if (UAString.indexOf("Trident") !== -1 && UAString.indexOf("rv:11") !== -1) {
	isIE =  true;
}
var IE = (function () {
	"use strict";

	var ret,
		isTheBrowser,
		actualVersion,
		jscriptMap,
		jscriptVersion;

	isTheBrowser = false;
	jscriptMap = {
		"5.5": "5.5",
		"5.6": "6",
		"5.7": "7",
		"5.8": "8",
		"9": "9",
		"10": "10"
	};
	jscriptVersion = new Function("/*@cc_on return @_jscript_version; @*/")();

	if (jscriptVersion !== undefined) {
		isTheBrowser = true;
		actualVersion = jscriptMap[jscriptVersion];
	}

	ret = {
		isTheBrowser: isTheBrowser,
		actualVersion: actualVersion
	};

	return ret;
}());
