define([
  "rok/config",
  "esri/request",
  "esri/map",
  "esri/layers/ArcGISDynamicMapServiceLayer",
  "esri/layers/FeatureLayer",
  "esri/InfoTemplate",
  "esri/dijit/Popup",
  "esri/dijit/PopupTemplate",
  "esri/dijit/PopupMobile",
  "ncam/PopupExtended",
  "esri/layers/DynamicLayerInfo",
  "esri/layers/LayerDataSource",
  "esri/layers/LayerDrawingOptions",
  "esri/layers/TableDataSource",
  "esri/Color",
  "esri/renderers/SimpleRenderer",
  "esri/renderers/UniqueValueRenderer",
  "esri/renderers/ClassBreaksRenderer",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",
  "dojo/_base/array",
], function (
  config,
  esriRequest,
  Map,
  ArcGISDynamicMapServiceLayer,
  FeatureLayer,
  InfoTemplate,
  Popup,
  PopupTemplate,
  PopupMobile,
  PopupExtended,
  DynamicLayerInfo,
  LayerDataSource,
  LayerDrawingOptions,
  TableDataSource,
  Color,
  SimpleRenderer,
  UniqueValueRenderer,
  ClassBreaksRenderer,
  SimpleFillSymbol,
  SimpleLineSymbol,
  arrayUtils
) {
  var that;
  return {
    createToc: function () {
      that = this;
      that.setUpDemographicLayers();

      $("#demobtn").click(function () {
        $(".demoLayersDiv").toggle();
        $(".standardLayersDiv").hide();
        $(".advancedLayersDiv").hide();
        $("#demobtn").toggleClass("btn-default-2 btn-default-2-close");
        if ($("#standardbtn").hasClass("btn-default-2-close"))
          $("#standardbtn").toggleClass("btn-default-2 btn-default-2-close");
        if ($("#advbtn").hasClass("btn-default-2-close"))
          $("#advbtn").toggleClass("btn-default-2 btn-default-2-close");
      });
      $("#standardbtn").click(function () {
        $(".demoLayersDiv").hide();
        $(".standardLayersDiv").toggle();
        $(".advancedLayersDiv").hide();
        if ($("#demobtn").hasClass("btn-default-2-close"))
          $("#demobtn").toggleClass("btn-default-2 btn-default-2-close");
        $("#standardbtn").toggleClass("btn-default-2 btn-default-2-close");
        if ($("#advbtn").hasClass("btn-default-2-close"))
          $("#advbtn").toggleClass("btn-default-2 btn-default-2-close");
      });
      $("#advbtn").click(function () {
        $(".demoLayersDiv").hide();
        $(".standardLayersDiv").hide();
        $(".advancedLayersDiv").toggle();
        if ($("#demobtn").hasClass("btn-default-2-close"))
          $("#demobtn").toggleClass("btn-default-2 btn-default-2-close");
        if ($("#standardbtn").hasClass("btn-default-2-close"))
          $("#standardbtn").toggleClass("btn-default-2 btn-default-2-close");
        $("#advbtn").toggleClass("btn-default-2 btn-default-2-close");
      });
      $(".clearall").click(function () {
        that.clearAllLayers();
      });
      $("#legendLayers").on("change", function () {
        that.updateLegend($("#legendLayers").val());
      });
      $(".vislayerlist").sortable({
        revert: true,
        update: function (event, ui) {
          that.reorderLayers();
        },
      });
    },
    setUpDemographicLayers: function () {
      var serviceRequest = esriRequest({
        useProxy: true,
        url: config.operationalMapService,
        content: { f: "json" },
        responseType: "json",
        handleAs: "json",
        callbackParamName: "callback",
      });
      serviceRequest.then(function (response) {
        var demoLayerInputs = [],
          standardLayerInputs = [],
          advInputLayers = [],
          layers = [];
        for (var i = 0; i < response.layers.length; i++) {
          if (config.demographicLayerIDs.indexOf(response.layers[i].id) != -1) {
            demoLayerInputs.push(
              "<label  title='" +
                response.layers[i].name +
                "'><input class='jsTogglevislayers' type='checkbox' name='operationalLayers' id='" +
                response.layers[i].id +
                "'> " +
                response.layers[i].name +
                "</input></label></br>"
            );
          }
          if (config.standardLayerIDs.indexOf(response.layers[i].id) != -1) {
            var name = response.layers[i].name.replace("Center", "Location");
            if (name == "Locations" || name == "Competitors") {
              standardLayerInputs.push(
                "<label  title='" +
                  name +
                  "'><input class='jsTogglevislayers' type='checkbox' checked name='operationalLayers' id='" +
                  response.layers[i].id +
                  "'> " +
                  name +
                  "</input></label></br>"
              );
              layers.push(
                '<li class="list-group-item toggleLayer" id="toggID_' +
                  response.layers[i].id +
                  '"><i class="fas fa-times"></i> ' +
                  name +
                  "</li>"
              );
            } else {
              standardLayerInputs.push(
                "<label  title='" +
                  name +
                  "'><input class='jsTogglevislayers' type='checkbox' name='operationalLayers' id='" +
                  response.layers[i].id +
                  "'> " +
                  name +
                  "</input></label></br>"
              );
            }
          }
        }
        $(".demoLayersDiv").append(demoLayerInputs);
        $(".standardLayersDiv").append(standardLayerInputs);
        $(".vislayerlist").append(layers);
        $(".jsTogglevislayers").on("click", function (e) {
          $("#8").on("change", function () {
            if ($("#8").is(":checked")) {
              $("#competitorsID").change(function () {
                if ($("#competitorsID option:selected").length === 0) {
                  operationalMapService.setVisibleLayers([]);
                } else {
                  operationalMapService.setVisibleLayers([8]);
                }
              });
            }
          });
          $("#46").on("change", function () {
            if ($("#46").is(":checked")) {
              centersFL.show();
            } else {
              centersFL.hide();
            }
          });
          $("#45").on("change", function () {
            if ($("#45").is(":checked")) {
              compCentersFL.show();
            } else {
              compCentersFL.hide();
            }
          });
          $("#legendLayers").empty();
          $(".legend").empty();
          var visible = [],
            layers = [],
            legendLayers = [],
            placeHolderNames = [];
          // if the layer is checked prepend it to the top of the list. Otherwise remove it
          if ($("#" + this.id).is(":checked")) {
            $(".vislayerlist").prepend(
              '<li class="list-group-item" id="toggID_' +
                this.id +
                '"><i id="toggID_' +
                this.id +
                '" class="fas fa-times toggleLayer"></i>' +
                this.labels[0].innerText +
                "</li>"
            );
          } else {
            $("#toggID_" + this.id).remove();
          }
          $.each($("input[name='operationalLayers']:checked"), function () {
            visible.push(this.id);
            layers.push(
              '<li class="list-group-item " id="toggID_' +
                this.id +
                '"><i id="toggID_' +
                this.id +
                '" class="fas fa-times toggleLayer"></i>' +
                this.labels[0].innerText +
                "</li>"
            );
            if (
              this.labels[0].innerText === " Locations" ||
              this.labels[0].innerText === " Competitors"
            ) {
            } else if (this.labels["0"].innerText === "Competitor Overlay") {
              legendLayers.push(
                '<option value="compOverlay">Competitor Overlay</option>'
              );
            } else if (this.labels["0"].innerText === "Block Group Winner") {
              legendLayers.push(
                '<option value="blkGroupWin">Block Group Winner</option>'
              );
            } else {
              var str = this.labels[0].innerText.substring(0, 8);
              var isTrue = !!placeHolderNames.find(function (elem) {
                return !!~str.indexOf(elem);
              });
              placeHolderNames.push(this.labels[0].innerText.substring(0, 8));

              if (isTrue !== true) {
                if (str.indexOf("Visitor") != -1) {
                  legendLayers.push(
                    '<option value="' +
                      this.id +
                      '">' +
                      this.labels[0].innerText.substring(0, 45) +
                      "</option>"
                  );
                } else if (str.indexOf(" Market ") != -1) {
                  legendLayers.push(
                    '<option value="' +
                      this.id +
                      '">' +
                      this.labels[0].innerText.substring(0, 19) +
                      "</option>"
                  );
                } else {
                  legendLayers.push(
                    '<option value="' +
                      this.id +
                      '">' +
                      this.labels[0].innerText +
                      "</option>"
                  );
                }
              }
            }
          });
          for (var info in infos) {
            var i = infos[info];
            if (visible.indexOf(i.id) !== -1) {
              i.visible = true; //!i.visible;
            } else {
              i.visible = false;
            }
          }
          var result1 = that.arrayRemove(visible, 46);
          var result2 = that.arrayRemove(result1, 45);
          visible = result2;
          operationalMapService.setVisibleLayers(result2);

          // when starting up, all of the competitors are intially selected. If the competitor layer is selected and the
          // number of competitors is greater than 1, send an error message.
          for (var i = 0; i < $("#vislayerlist").children().length; i++) {
            var selectedOptions = $("#competitorsID option:selected").length;
            var layerChildId = $("#vislayerlist").children()[i].id;
            if (
              layerChildId === "toggID_49" ||
              layerChildId === "toggID_48" ||
              $("#competitorOverlayChk").prop("checked")
            ) {
              if (selectedOptions > 1 && selectedOptions !== null) {
                swal({
                  title: "Error Encountered",
                  text: "Only one competitor should be selected!",
                  icon: "error",
                });
              }
            }
          }
          $("#legendLayers").append(legendLayers);
          $("#legendLayers").trigger("change");
          that.reorderLayers();
          if (
            visible.indexOf(config.competitorMarketPenetration) != -1 ||
            visible.indexOf(config.competitorShopperOrigins) != -1
          ) {
            $("#showMaxDevice").attr("disabled", false);
          } else {
            $("#showMaxDevice").attr("disabled", true);
          }
          $(".toggleLayer").on("click", function (e) {
            var layerID = e.currentTarget.id.replace("toggID_", "");
            $("#" + e.currentTarget.id).remove();
            if (layerID == "compOverlay") {
              competitorOverlayFL.hide();
              document.getElementById("competitorOverlayChk").checked = false;
              if ($("#legendLayers").val() == layerID) {
                $("#legendDynamicLayers").hide();
              }
            } else if (layerID == "shopperOrigin") {
              shopperOriginFL.hide();
              document.getElementById("shopperOrigin").checked = false;
              if ($("#legendLayers").val() == layerID) {
                $("#legendDynamicLayers").hide();
              }
            } else if (layerID == "zipCodeChk") {
              map._layers.zipCodeLayer.hide();
              document.getElementById("zipCodeChk").checked = false;
              if ($("#legendLayers").val() == layerID) {
                $("#legendDynamicLayers").hide();
              }
            } else {
              document.getElementById(layerID).checked = false;
              var result = that.arrayRemove(visible, layerID);
              var result1 = that.arrayRemove(result, 46);
              var result2 = that.arrayRemove(result1, 45);
              visible = result2;
              operationalMapService.setVisibleLayers(result2);
              if (layerID == 0) {
                centersFL.hide();
              }
              if (layerID == 1) {
                compCentersFL.hide();
              }
              if ($("#legendLayers").val() == layerID) {
                $(".legend").empty();
              }
            }
            $("#legendLayers option[value='" + layerID + "']").remove();
          });
        });
      });
    },
    clearAllLayers: function () {
      $(".vislayerlist").empty();
      operationalMapService.setVisibleLayers([]);
      $("input[name='operationalLayers']").prop("checked", false);
    },
    arrayRemove: function (arr, value) {
      return arr.filter(function (ele) {
        return ele != value;
      });
    },
    arrayContains: function (windowArray, testVal) {
      var index, value, result;
      for (index = 0; index < windowArray.length; ++index) {
        value = windowArray[index];
        if (value.substring(0, 8) === testVal) {
          // You've found it, the full text is in `value`.
          // So you might grab it and break the loop, although
          // really what you do having found it depends on
          // what you need.
          return true;
        }
      }
    },
    updateLegend: function (layer) {
      $(".legend").empty();
      $("#legendDynamicLayers").hide();
      if (layer == "" || layer === "2") {
        return;
      } else if (layer == "compOverlay") {
        compOverlayControl.showCompetitorOverlayLegend();
        $("#legendDynamicLayers").show();
      } else if (layer == "blkGroupWin") {
        blockGroupWinControl.showBlockGroupLegend();
        $("#legendDynamicLayers").show();
      } else if (layer == "zipCodeChk") {
        zipCodeControl.showZipCodeLegend();
        $("#legendDynamicLayers").show();
      } else {
        var legendRequest = esriRequest({
          useProxy: true,
          url: config.operationalMapService + "/legend",
          content: { f: "pjson" },
          responseType: "pjson",
          handleAs: "json",
          callbackParamName: "callback",
        });
        legendRequest.then(function (response) {
          legendCalled = true;
          for (var l = 0; l < response.layers.length; l++) {
            if (response.layers[l].layerId == layer) {
              if (response.layers[l].legend.length > 1) {
                for (var m = 0; m < response.layers[l].legend.length; m++) {
                  var imgURL =
                    config.operationalMapService +
                    "/" +
                    response.layers[l].layerId +
                    "/images/" +
                    response.layers[l].legend[m].url;
                  var imgLabel = response.layers[l].legend[m].label;
                  $(".legend").append(
                    "<p><img src=" + imgURL + ">" + imgLabel + "</p>"
                  );
                }
              } else {
                var imgURL =
                  config.operationalMapService +
                  "/" +
                  response.layers[l].layerId +
                  "/images/" +
                  response.layers[l].legend[0].url;
                var imgLabel = response.layers[l].legend[0].label;
                $(".legend").append(
                  "<p><img src=" + imgURL + ">" + imgLabel + "</p>"
                );
              }
            }
          }
        });
      }
    },
    reorderLayers: function () {
      var newOrder = that.getVisibleLayers();

      var visible = [];
      for (var i = 0; i < newOrder.length; i++) {
        if (newOrder) break;
        if (newOrder[i].id != 0 && newOrder[i].id != 1) {
          newOrder[i].defaultVisibility = true;
          visible.push(newOrder[i]);
        }
      }
      operationalMapService.setDynamicLayerInfos(visible);
    },
    getVisibleLayers: function () {
      // get layer name nodes, build an array corresponding to new layer order
      var layerOrder = [];
      $(".vislayerlist li").each(function (n, idx) {
        for (var info in infos) {
          var i = infos[info];
          if (i.name === idx.innerText.replace(" ", "")) {
            layerOrder[n] = i.id;
            // keep track of a layer's position in the layer list
            i.position = n;
            break;
          }
        }
      });

      // find the layer IDs for visible layer
      var ids = arrayUtils.filter(layerOrder, function (l) {
        if (l) {
          return infos[l].visible;
        }
      });
      // get the dynamicLayerInfos for visible layers
      var visible = arrayUtils.map(ids, function (id) {
        return dynamicLayerInfos[id];
      });
      return visible;
    },
  };
});
