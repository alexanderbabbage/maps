define([
  "rok/config",
  "esri/request",
  "esri/geometry/geometryEngine",
  "esri/tasks/query",
  "esri/tasks/QueryTask",
  "esri/tasks/IdentifyTask",
  "esri/tasks/IdentifyParameters",
  "esri/SpatialReference",
  "esri/graphicsUtils",
  "esri/InfoTemplate",
  "esri/layers/FeatureLayer",
  "esri/tasks/BufferParameters",
  "esri/dijit/Popup",
  "esri/dijit/PopupTemplate",
  "dojo/_base/array",
  "dijit/form/HorizontalSlider",
  "dojo/dom"
], function(
  config,
  esriRequest,
  geometryEngine,
  Query,
  QueryTask,
  IdentifyTask,
  IdentifyParameters,
  SpatialReference,
  graphicsUtils,
  InfoTemplate,
  FeatureLayer,
  BufferParameters,
  Popup,
  PopupTemplate,
  arrayUtils,
  HorizontalSlider,
  dom
) {
  var tvInfoTemplate = new PopupTemplate({
    title: "TV Coverage",
    fieldInfos: [
      { fieldName: "Callsign", label: "Call Sign", visible: true },
      { fieldName: "Licensee", label: "Licensee", visible: true },
      { fieldName: "CITY", label: "CITY", visible: true },
      { fieldName: "STATE", label: "STATE", visible: true }
    ],
    extended: {
      actions: [
        {
          text: "Zoom to",
          className: "defaultAction",
          title: "Zoom to selected feature.",
          click: function(feature) {
            zoomtoFromInfoWindow(feature);
          }
        }
      ],
      themeClass: "extended", //uses a pretty bad custom theme defined in PopupExtended.css.
      scaleSelected: 0
    }
  });
  var radioInfoTemplate = new PopupTemplate({
    title: "Radio Coverage",
    fieldInfos: [
      { fieldName: "Callsign", label: "Call Sign", visible: true },
      { fieldName: "Service", label: "Service", visible: true },
      { fieldName: "Frequency", label: "Frequency", visible: true },
      { fieldName: "City", label: "City", visible: true },
      { fieldName: "State", label: "State", visible: true }
    ],
    extended: {
      actions: [
        {
          text: "Zoom to",
          className: "defaultAction",
          title: "Zoom to selected feature.",
          click: function(feature) {
            zoomtoFromInfoWindow(feature);
          }
        }
      ],
      themeClass: "extended", //uses a pretty bad custom theme defined in PopupExtended.css.
      scaleSelected: 0
    }
  });
  var tvCoverageInfoTemplate = new PopupTemplate({
    title: "TV Coverage",
    fieldInfos: [
      { fieldName: "Callsign", label: "Call Sign", visible: true },
      { fieldName: "Licensee", label: "Licensee", visible: true },
      { fieldName: "CITY", label: "CITY", visible: true },
      { fieldName: "STATE", label: "STATE", visible: true }
    ],
    extended: {
      actions: [
        {
          text: "Zoom to",
          className: "defaultAction",
          title: "Zoom to selected feature.",
          click: function(feature) {
            zoomtoFromInfoWindow(feature);
          }
        }
      ],
      themeClass: "extended", //uses a pretty bad custom theme defined in PopupExtended.css.
      scaleSelected: 0
    }
  });
  var radioCoverageInfoTemplate = new PopupTemplate({
    title: "Radio Coverage",
    fieldInfos: [
      { fieldName: "Call", label: "Call Sign", visible: true },
      { fieldName: "Service", label: "Service", visible: true },
      { fieldName: "Channel", label: "Channel", visible: true },
      { fieldName: "City", label: "City", visible: true },
      { fieldName: "State", label: "State", visible: true }
    ],
    extended: {
      actions: [
        {
          text: "Zoom to",
          className: "defaultAction",
          title: "Zoom to selected feature.",
          click: function(feature) {
            zoomtoFromInfoWindow(feature);
          }
        }
      ],
      themeClass: "extended", //uses a pretty bad custom theme defined in PopupExtended.css.
      scaleSelected: 0
    }
  });
  var tvFeatureLayer = new FeatureLayer(
    config.radioTVLayers + "/" + config.tvID,
    {
      mode: FeatureLayer.MODE_ONDEMAND,
      infoTemplate: tvInfoTemplate,
      outFields: ["Callsign", "Licensee", "City", "State"],
      opacity: 1
    }
  );
  var radioFeatureLayer = new FeatureLayer(
    config.radioTVLayers + "/" + config.radioID,
    {
      mode: FeatureLayer.MODE_ONDEMAND,
      infoTemplate: radioInfoTemplate,
      outFields: ["Callsign", "Service", "Frequency", "City", "State"],
      opacity: 1
    }
  );
  var tvCoverageFeatureLayer = new FeatureLayer(
    config.radioTVLayers + "/" + config.tvCoverageID,
    {
      mode: FeatureLayer.MODE_ONDEMAND,
      infoTemplate: tvCoverageInfoTemplate,
      outFields: ["CALL", "LICENSEE", "CITY", "STATE"],
      opacity: 0.5
    }
  );
  var radioCoverageFeatureLayer = new FeatureLayer(
    config.radioTVLayers + "/" + config.radioCoverageID,
    {
      mode: FeatureLayer.MODE_ONDEMAND,
      infoTemplate: radioCoverageInfoTemplate,
      outFields: ["Call", "Service", "Channel", "City", "State"],
      opacity: 0.5
    }
  );
  //Create Slider to increase buffer
  var slider = new HorizontalSlider(
    {
      showButtons: false,
      value: 0,
      intermediateChanges: false,
      style: "width:70%;padding:0 20px 0 20px",
      tooltip: "Distance Around Center",
      minimum: 0,
      maximum: 50,
      discreteValues: 4,
      onChange: function(value) {
        if (value == 16.666666666666668) {
          that.createTVRadioLayers(mapForSlider, centerForSlider, 10);
        }
        if (value == 33.333333333333336) {
          that.createTVRadioLayers(mapForSlider, centerForSlider, 25);
        } else {
          that.createTVRadioLayers(mapForSlider, centerForSlider, value);
        }
      },
      layoutAlign: "right"
    },
    "mediaLayerSlider"
  );
  var mapForSlider, centerForSlider;
  return {
    createTVRadioLayers: function(map, centerID, bufferDistance) {
      (that = this), (mapForSlider = map), (centerForSlider = centerID);
      map.removeLayer(tvFeatureLayer);
      map.removeLayer(tvCoverageFeatureLayer);
      map.removeLayer(radioFeatureLayer);
      map.removeLayer(radioCoverageFeatureLayer);
      $("#populationReached").empty();
      if (bufferDistance == 0) {
        identify = new IdentifyTask(config.radioTVLayers);
        identifyParams = new IdentifyParameters();
        identifyParams.layerIds = [config.tvCoverageID, config.radioCoverageID]; //config.selectLayerIDs;
        identifyParams.returnGeometry = false;
        identifyParams.geometry = centersFL.graphics[0].geometry; //result.features[0].geometry;
        identifyParams.tolerance = 3;
        identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
        identifyParams.mapExtent = map.extent;
        identify.execute(identifyParams, that.createLayers);
      } else {
        var geometries = graphicsUtils.getGeometries(centersFL.graphics);
        var buffered = geometryEngine.buffer(
          geometries,
          [bufferDistance],
          9093,
          true
        );

        identify = new IdentifyTask(config.radioTVLayers);
        identifyParams = new IdentifyParameters();
        identifyParams.layerIds = [config.tvCoverageID, config.radioCoverageID]; //config.selectLayerIDs;
        identifyParams.returnGeometry = false;
        identifyParams.geometry = buffered[0]; //result.features[0].geometry;
        identifyParams.tolerance = 3;
        identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
        identifyParams.mapExtent = map.extent;
        identify.execute(identifyParams, that.createLayers);
      }
    },
    createLayers: function(results) {
      $("#mediaDIV").empty();
      $("#tvDIV1").empty();
      $("#radioDIV1").empty();
      $("#tvDIV2").empty();
      $("#radioDIV2").empty();
      if (results.length == 0) {
        $("#mediaCoverageChkDiv").append("<h4>No media coverage");
        $("#mediaCoverageChkDiv").show();
      } else {
        var radio1 = [],
          radio2 = [],
          tv1 = [],
          tv2 = [];
        $("#mediaDIV").append(
          "<br><label><input id='allmedia' type='checkbox' >Media Layers</input></label>"
        );
        for (var i = 0; i < results.length; i++) {
          if (results[i].layerId == config.tvCoverageID) {
            if (i & 1) {
              tv1.push(
                "<label><input class='jsToggleTVRadiolayers' name='tvLayers' id='" +
                  results[i].feature.attributes.ID0 +
                  "' type='checkbox'>" +
                  results[i].value +
                  "</input></label></br>"
              );
            } else {
              tv2.push(
                "<label><input class='jsToggleTVRadiolayers' name='tvLayers' id='" +
                  results[i].feature.attributes.ID0 +
                  "' type='checkbox'>" +
                  results[i].value +
                  "</input></label></br>"
              );
            }
          }
          if (results[i].layerId == config.radioCoverageID) {
            if (i & 1) {
              radio2.push(
                "<label><input class='jsToggleTVRadiolayers' name='radioLayers' id='" +
                  results[i].value +
                  "' type='checkbox'>" +
                  results[i].value +
                  "</input></label></br>"
              );
            } else {
              radio1.push(
                "<label><input class='jsToggleTVRadiolayers' name='radioLayers' id='" +
                  results[i].value +
                  "' type='checkbox'>" +
                  results[i].value +
                  "</input></label></br>"
              );
            }
          }
        }
        var uniqueTV1 = [];
        $.each(tv1, function(i, el) {
          if ($.inArray(el, uniqueTV1) === -1) uniqueTV1.push(el);
        });
        $("#tvDIV1").append(uniqueTV1.sort());
        var uniqueTV2 = [];
        $.each(tv2, function(i, el) {
          if ($.inArray(el, uniqueTV2) === -1) uniqueTV2.push(el);
        });
        $("#tvDIV2").append(uniqueTV2.sort());
        var uniqueRadio1 = [];
        $.each(radio1, function(i, el) {
          if ($.inArray(el, uniqueRadio1) === -1) uniqueRadio1.push(el);
        });
        $("#radioDIV1").append(uniqueRadio1.sort());
        var uniqueRadio2 = [];
        $.each(radio2, function(i, el) {
          if ($.inArray(el, uniqueRadio2) === -1) uniqueRadio2.push(el);
        });
        $("#radioDIV2").append(uniqueRadio2.sort());
        tvFeatureLayer.setDefinitionExpression("1=2");
        radioFeatureLayer.setDefinitionExpression("1=2");
        tvCoverageFeatureLayer.setDefinitionExpression("1=2");
        radioCoverageFeatureLayer.setDefinitionExpression("1=2");
        map.addLayers([
          tvCoverageFeatureLayer,
          radioCoverageFeatureLayer,
          tvFeatureLayer,
          radioFeatureLayer
        ]);
      }
      $("#mediaCoverageChkDiv").show();
      $("#mediaCoverageBtn").button("reset");
      $("#allmedia").on("change", function(e) {
        if ($("#allmedia").is(":checked")) {
          var visible = [],
            visibleR = [];
          $(".jsToggleTVRadiolayers").prop("checked", true);
          $.each($("input[name='tvLayers']:checked"), function() {
            visible.push("'" + this.id + "'");
          });
          tvFeatureLayer.setDefinitionExpression("Fac_ID IN (" + visible + ")");
          tvCoverageFeatureLayer.setDefinitionExpression(
            "ID0 IN (" + visible + ")"
          );
          $.each($("input[name='radioLayers']:checked"), function() {
            visibleR.push("'" + this.id + "'");
          });
          radioFeatureLayer.setDefinitionExpression(
            "Callsign IN (" + visibleR + ")"
          );
          radioCoverageFeatureLayer.setDefinitionExpression(
            "Call IN (" + visibleR + ")"
          );
          tvFeatureLayer.show();
          tvCoverageFeatureLayer.show();
          radioFeatureLayer.show();
          radioCoverageFeatureLayer.show();
        } else {
          tvFeatureLayer.hide();
          tvCoverageFeatureLayer.hide();
          radioFeatureLayer.hide();
          radioCoverageFeatureLayer.hide();
          $(".jsToggleTVRadiolayers").prop("checked", false);
          $("#populationReached").empty();
        }
      });
      $(".jsToggleTVRadiolayers").on("click", function(e) {
        var visible = [];
        $.each($("input[name='tvLayers']:checked"), function() {
          visible.push("'" + this.id + "'");
        });
        if (visible.length != 0) {
          tvFeatureLayer.setDefinitionExpression("Fac_ID IN (" + visible + ")");
          tvCoverageFeatureLayer.setDefinitionExpression(
            "ID0 IN (" + visible + ")"
          );
          tvFeatureLayer.show();
          tvCoverageFeatureLayer.show();
          $("#allmedia").prop("checked", true);
        } else {
          tvFeatureLayer.hide();
          tvCoverageFeatureLayer.hide();
        }
        var visibleR = [];
        $.each($("input[name='radioLayers']:checked"), function() {
          visibleR.push("'" + this.id + "'");
        });
        if (visibleR.length != 0) {
          radioFeatureLayer.setDefinitionExpression(
            "Callsign IN (" + visibleR + ")"
          );
          radioCoverageFeatureLayer.setDefinitionExpression(
            "Call IN (" + visibleR + ")"
          );
          radioFeatureLayer.show();
          radioCoverageFeatureLayer.show();
          $("#allmedia").prop("checked", true);
        } else {
          radioFeatureLayer.hide();
          radioCoverageFeatureLayer.hide();
        }
        $("#populationReached").empty();
        if (visible.length == 0 && visibleR.length == 0) {
          $("#allmedia").prop("checked", false);
        }
        $("#getPopulationBtn").show();
      });
    },
    removeMediaCoverageLayers: function() {
      map.removeLayer(tvFeatureLayer);
      map.removeLayer(tvCoverageFeatureLayer);
      map.removeLayer(radioFeatureLayer);
      map.removeLayer(radioCoverageFeatureLayer);
      $("#getPopulationBtn").hide();
      $("#populationReached").empty();
      $("#mediaCoverageChkDiv").hide();
      $("#mediaCoverageBtn").removeClass("btn-info");
      $("#mediaCoverageBtn").addClass("btn-primary");
      $("#mediaCoverageChkDiv").removeClass("layercreated");
    },
    getShopperPopWithInTV: function() {
      $("#populationReached").empty();
      var tvObIDs = [],
        radioObIDs = [];
      if(tvCoverageFeatureLayer.graphics.length === 0) tvObIDs.push("0");
      if(radioCoverageFeatureLayer.graphics.length === 0) radioObIDs.push("0");

      for (var i = 0; i < tvCoverageFeatureLayer.graphics.length; i++) {
        tvObIDs.push(tvCoverageFeatureLayer.graphics[i].attributes.OBJECTID);
      }

      for (var i = 0; i < radioCoverageFeatureLayer.graphics.length; i++) {
        radioObIDs.push(
          radioCoverageFeatureLayer.graphics[i].attributes.OBJECTID
        );
      }

      $.ajax({
        method: "POST",
        url: "https://trutradedevelop.alexanderbabbage.com:5000/maps/getTotalPop",
        data: { centerForSlider: centerForSlider, tvObIDs: tvObIDs, radioObIDs: radioObIDs}
      })
        .done(function(res) {
          res = res[0];
          function addCommas(nStr) {
            nStr += "";
            x = nStr.split(".");
            x1 = x[0];
            x2 = x.length > 1 ? "." + x[1] : "";
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
              x1 = x1.replace(rgx, "$1" + "," + "$2");
            }
            return x1 + x2;
          }
          $("#populationReached").append(
            "<label>Total Population Reached:</label> " +
              addCommas(res.TotalPopulationReached) +
              "<br>"
          );
          $("#populationReached").append(
            "<label>Total Visitors Reached:</label> " +
              addCommas(res.TotalShoppersReached) +
              "<br>"
          );
          $("#populationReached").append(
            "<label>% Visitor Origin Reached:</label> " +
              res.PercentShopperOriginReached +
              "<br>"
          );
          $("#populationReached").append(
            "<label>Visitor Origin Population:</label> " +
              addCommas(res.CenterPopulation) +
              "<br>"
          );
          $("#getPopulationBtn").hide();
          $("#getPopulationBtn").button("reset");
        },
        "json"
      );
    }
  };
});
