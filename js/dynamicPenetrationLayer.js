define([
  "rok/config",
  "esri/map",
  "esri/layers/ArcGISDynamicMapServiceLayer",
  "esri/layers/FeatureLayer",
  "esri/InfoTemplate",
  "esri/layers/DynamicLayerInfo",
  "esri/layers/LayerDataSource",
  "esri/layers/LayerDrawingOptions",
  "esri/layers/TableDataSource",
  "esri/Color",
  "esri/renderers/SimpleRenderer",
  "esri/renderers/ClassBreaksRenderer",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol"
], function(
  config,
  Map,
  ArcGISDynamicMapServiceLayer,
  FeatureLayer,
  InfoTemplate,
  DynamicLayerInfo,
  LayerDataSource,
  LayerDrawingOptions,
  TableDataSource,
  Color,
  SimpleRenderer,
  ClassBreaksRenderer,
  SimpleFillSymbol,
  SimpleLineSymbol
) {
  return {
    createPenetrationLayer: function(map, dynamicMapService, tableName) {
      var layerName = "Simon.DBO." + tableName; //"Simon.DBO.TempBlkGrpDevices";
      // create a table data source to access the lakes layer
      var dataSource = new TableDataSource();
      dataSource.workspaceId = "babbageID"; // not exposed via REST :(
      dataSource.dataSourceName = layerName;
      // and now a layer source
      var layerSource = new LayerDataSource();
      layerSource.dataSource = dataSource;

      //Create Symbols for Renderer
      var outlineColor = new dojo.Color([255, 255, 190, 0]);
      var symLine = new esri.symbol.SimpleLineSymbol().setColor(outlineColor);

      var symDefault = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([0, 0, 0, 0])
      );
      var symBreak10 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([34, 102, 51, 1])
      );
      var symBreak9 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([50, 115, 65, 1])
      );
      var symBreak8 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([69, 130, 82, 1])
      );
      var symBreak7 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([87, 145, 101, 1])
      );
      var symBreak6 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([108, 161, 120, 1])
      );
      var symBreak5 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([127, 176, 139, 1])
      );
      var symBreak4 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([149, 194, 162, 1])
      );
      var symBreak3 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([171, 209, 184, 1])
      );
      var symBreak2 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([195, 227, 208, 1])
      );
      var symBreak1 = new esri.symbol.SimpleFillSymbol(
        "solid",
        symLine,
        new dojo.Color([220, 245, 233, 1])
      );

      //Create Renderer
      var cbr = new ClassBreaksRenderer("", "Devices");
      cbr.addBreak({
        minValue: 1,
        maxValue: 25,
        symbol: symBreak1,
        label: "Weakest Penetration"
      });
      cbr.addBreak({
        minValue: 26,
        maxValue: 90,
        symbol: symBreak2,
        label: "2"
      });
      cbr.addBreak({
        minValue: 91,
        maxValue: 208,
        symbol: symBreak3,
        label: "3"
      });
      cbr.addBreak({
        minValue: 209,
        maxValue: 403,
        symbol: symBreak4,
        label: "4"
      });
      cbr.addBreak({
        minValue: 404,
        maxValue: 727,
        symbol: symBreak5,
        label: "5"
      });
      cbr.addBreak({
        minValue: 728,
        maxValue: 1220,
        symbol: symBreak6,
        label: "6"
      });
      cbr.addBreak({
        minValue: 1221,
        maxValue: 1988,
        symbol: symBreak7,
        label: "7"
      });
      cbr.addBreak({
        minValue: 1989,
        maxValue: 3432,
        symbol: symBreak8,
        label: "8"
      });
      cbr.addBreak({
        minValue: 3433,
        maxValue: 5991,
        symbol: symBreak9,
        label: "9"
      });
      cbr.addBreak({
        minValue: 5992,
        maxValue: 9855,
        symbol: symBreak10,
        label: "Strongest Penetration"
      });

      //Create Feature layer and Infowindow
      var content =
        "<b>Total Devices</b>: ${Devices}" + "<br><b>GeoID</b>: ${Geoid}";
      var infoTemplate = new InfoTemplate("Dynamic Layer", content);

      featureLayer = new FeatureLayer(
        config.operationalMapService + "/dynamicLayer",
        {
          mode: FeatureLayer.MODE_ONDEMAND,
          outFields: ["*"],
          objectIdField: "ObjectID",
          source: layerSource,
          infoTemplate: infoTemplate,
          id: "pentetrationLayer",
          opacity: dynamicMapService.opacity,
          visible: false
        }
      );
      featureLayer.setRenderer(cbr);
      featureLayer.setAutoGeneralize(true);
      map.addLayer(featureLayer);
      legend.refresh([
        { layer: featureLayer, title: "Market Penetration" },
        { layer: dynamicMapService, title: "Operational Layers" }
      ]);
    }
  };
});
