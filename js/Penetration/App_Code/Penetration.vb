﻿Imports System.Web
Imports System.Collections
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

Imports System
Imports System.IO
Imports System.Text
Imports System.Collections.Generic

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://34.202.136.200:8080/TruTrade")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Penetration
    <System.Web.Services.WebMethod(EnableSession:=True)> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function GetPenetrationData(ByVal sCenter As String) As String
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand
        Dim connectionString As String
        Dim conn As SqlConnection
        connectionString = ConfigurationManager.ConnectionStrings("SimonDB").ConnectionString
        conn = New SqlConnection(connectionString)

        Dim cmd1 As SqlCommand = New SqlCommand("SP_Penetration", conn)
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.Parameters.Add("@Center", SqlDbType.VarChar, 1000, ParameterDirection.Input).Value = sCenter
        cmd1.Parameters.Add("@Tablename", SqlDbType.VarChar, 50)
        cmd1.Parameters("@Tablename").Direction = ParameterDirection.Output
        conn.Open()
        cmd1.ExecuteNonQuery()
        conn.Close()

        Dim sOutput As String = cmd1.Parameters("@Tablename").Value.ToString()



        'ds = New System.Data.DataSet
        'da.MissingSchemaAction = MissingSchemaAction.AddWithKey
        'da.Fill(ds, "Penetration")


        ''da.Fill(ds)
        'Dim MArray()() As String = New String(ds.Tables(0).Rows.Count)() {}
        'Dim i As Integer = 0

        'For Each rs As DataRow In ds.Tables(0).Rows
        '    MArray(i) = New String() {rs("objectid").ToString(), rs("geoid").ToString(), rs("devices").ToString()}
        '    i = i + 1
        'Next

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        js.MaxJsonLength = 2147483644
        Dim sJSON As String = js.Serialize(sOutput)
        sJSON = Replace(sJSON, ",null", "")

        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(sJSON)

        ''Dim mydocpath As String = "D:\Internet\maps.roktech.net\CTRC\data"
        ''Using outfile As New StreamWriter(mydocpath & "\clinics.json")
        ''    outfile.Write(sb.ToString())
        ''End Using
        Return sJSON
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetHighest(ByVal sCenter As String) As String
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand
        Dim connectionString As String
        Dim conn As SqlConnection
        connectionString = ConfigurationManager.ConnectionStrings("SimonDB").ConnectionString
        conn = New SqlConnection(connectionString)

        Dim cmd1 As SqlCommand = New SqlCommand("SP_Highest", conn)
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.Parameters.Add("@Center", SqlDbType.VarChar, 1000, ParameterDirection.Input).Value = sCenter
        cmd1.Parameters.Add("@Tablename", SqlDbType.VarChar, 50)
        cmd1.Parameters("@Tablename").Direction = ParameterDirection.Output
        conn.Open()
        cmd1.ExecuteNonQuery()
        conn.Close()

        Dim sOutput As String = cmd1.Parameters("@Tablename").Value.ToString()

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        js.MaxJsonLength = 2147483644
        Dim sJSON As String = js.Serialize(sOutput)
        sJSON = Replace(sJSON, ",null", "")

        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(sJSON)

        Return sJSON
    End Function
    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetHighestZip(ByVal sCenter As String) As String
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand
        Dim connectionString As String
        Dim conn As SqlConnection
        connectionString = ConfigurationManager.ConnectionStrings("SimonDB").ConnectionString
        conn = New SqlConnection(connectionString)

        Dim cmd1 As SqlCommand = New SqlCommand("SP_Highest", conn)
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.Parameters.Add("@Center", SqlDbType.VarChar, 1000, ParameterDirection.Input).Value = sCenter
        cmd1.Parameters.Add("@Tablename", SqlDbType.VarChar, 50)
        cmd1.Parameters("@Tablename").Direction = ParameterDirection.Output
        conn.Open()
        cmd1.ExecuteNonQuery()
        conn.Close()

        Dim sOutput As String = cmd1.Parameters("@Tablename").Value.ToString()

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        js.MaxJsonLength = 2147483644
        Dim sJSON As String = js.Serialize(sOutput)
        sJSON = Replace(sJSON, ",null", "")

        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(sJSON)

        Return sJSON
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetHighestPOI(ByVal sCenter As String, sCenterID As String) As String
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand
        Dim connectionString As String
        Dim conn As SqlConnection
        connectionString = ConfigurationManager.ConnectionStrings("SimonDB").ConnectionString
        conn = New SqlConnection(connectionString)

        Dim cmd1 As SqlCommand = New SqlCommand("SP_HighestPOI", conn)
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.Parameters.Add("@Center", SqlDbType.VarChar, 1000, ParameterDirection.Input).Value = sCenter
        cmd1.Parameters.Add("@CenterID", SqlDbType.VarChar, 1000, ParameterDirection.Input).Value = sCenterID
        cmd1.Parameters.Add("@Tablename", SqlDbType.VarChar, 50)
        cmd1.Parameters("@Tablename").Direction = ParameterDirection.Output
        conn.Open()
        cmd1.ExecuteNonQuery()
        conn.Close()

        Dim sOutput As String = cmd1.Parameters("@Tablename").Value.ToString()

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        js.MaxJsonLength = 2147483644
        Dim sJSON As String = js.Serialize(sOutput)
        sJSON = Replace(sJSON, ",null", "")

        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(sJSON)

        Return sJSON
    End Function

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)>
    Public Function GetOverlay(ByVal sCenter As String, sCenterID As String) As String
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter
        Dim cmd As New SqlCommand
        Dim connectionString As String
        Dim conn As SqlConnection
        connectionString = ConfigurationManager.ConnectionStrings("SimonDB").ConnectionString
        conn = New SqlConnection(connectionString)

        Dim cmd1 As SqlCommand = New SqlCommand("SP_CompetitorOverlay", conn)
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.Parameters.Add("@Center", SqlDbType.VarChar, 1000, ParameterDirection.Input).Value = sCenter
        cmd1.Parameters.Add("@CenterID", SqlDbType.VarChar, 1000, ParameterDirection.Input).Value = sCenterID
        cmd1.Parameters.Add("@Tablename", SqlDbType.VarChar, 50)
        cmd1.Parameters("@Tablename").Direction = ParameterDirection.Output
        conn.Open()
        cmd1.ExecuteNonQuery()
        conn.Close()

        Dim sOutput As String = cmd1.Parameters("@Tablename").Value.ToString()

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        js.MaxJsonLength = 2147483644
        Dim sJSON As String = js.Serialize(sOutput)
        sJSON = Replace(sJSON, ",null", "")

        Dim sb As StringBuilder = New StringBuilder()
        sb.Append(sJSON)

        Return sJSON
    End Function

End Class