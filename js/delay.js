$.fn.typePause = function (dataObject, eventFunc)
{
	if(typeof dataObject === 'function')
	{
		eventFunc = dataObject;
		dataObject = {};
	}
	if(typeof dataObject.milliseconds === 'undefined')
	dataObject.milliseconds = 500;
	$(this).data('timeout', null)
	.keypress(dataObject, function(e)
	{
		clearTimeout($(this).data('timeout'));
		$(this).data('timeout', setTimeout($.proxy(eventFunc, this, e), dataObject.milliseconds));
	})
	.keyup(dataObject, function(e)
	{
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 8 || code == 46 || code == 13)
		$(this).triggerHandler('keypress',dataObject);
	});
}
