//create Generic Table
function createGenericTable(results, layername){
	var headers = [];
	$.each(results.fieldAliases,function(clientIdx,item){
	     headers.push([clientIdx, item]);
		 //rows.push(value.attributes );
	})

		var table = "<table>";
	table += "<tr><td><h5>Feature Details: "+layername+"</h5></td></tr>";
	var i = 0;
	for(attrValue in results.features[0].attributes){
		if(attrValue.toUpperCase().indexOf('CENTERID') != -1 || attrValue.toUpperCase().indexOf('OBJECTID') != -1 || attrValue.toUpperCase().indexOf('SHAPE') != -1){
			//do nothing
		}
		else{
			if(headers[i][1].indexOf("Percent") != -1){
				table += "<tr><td><b>"+headers[i][1]+":</b> "+formatPercent(results.features[0].attributes[attrValue])+"</td></tr>";
			}
			else if(results.fields[i].type == "esriFieldTypeDouble"){
				table += "<tr><td><b>"+headers[i][1]+":</b> "+formatNumber(results.features[0].attributes[attrValue])+"</td></tr>";
			}
			else{
				table += "<tr><td><b>"+headers[i][1]+":</b> "+results.features[0].attributes[attrValue]+"</td></tr>";
			}
		}
		i++
	}
	table += "<tr><td><a type='button' class='directionsBtn btn btn-default' id="+results.features[0].attributes.OBJECTID+" target='_blank'>Get Directions</a></tr>";
	table += "</table>";

	return table
}
//percent formatter
function formatPercent(value){
	var pctValue = value*100;
	return pctValue.toFixed(1)+ "%";
}
//form at number
function formatNumber(value){
	var numValue = '';
	if(value != null) numValue = value.toFixed(0);
	return numValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
//Date Formatter
function myDateFormatter (dateObject) {
	var d = new Date(dateObject);
	var day = d.getDate();
	var month = d.getMonth() + 1;
	var year = d.getFullYear();
	if (day < 10) {
		day = "0" + day;
	}
	if (month < 10) {
		month = "0" + month;
	}
	var date = month + "/" + day + "/" + year;

	return date;
};
