define([
         "rok/config", "dojo/number",
        "esri/InfoTemplate",
        "esri/layers/FeatureLayer",
        "esri/map",
        "esri/renderers/HeatmapRenderer",
        "dojo/domReady!"
      ], function (config, number, InfoTemplate, FeatureLayer, Map, HeatmapRenderer){
      	
	  return { 
	 	createHotSpotLayer: function(){
	 		/*map.on('load', function(evt){
        	var curentLevel = map.getLevel();
        	document.getElementById('mapscale').innerHTML = curentLevel;
	        })
	        map.on('zoom-end', function(evt){
	        	var curentLevel = map.getLevel();
	        	document.getElementById('mapscale').innerHTML = curentLevel;
	        })*/
	        // --------------------------------------------------------------------
	        // Format the magnitude value in the pop up to show one decimal place.
	        // Uses the dojo/number module to perform formatting.
	        // --------------------------------------------------------------------
	        formatMagnitude = function (value, key, data){
	          return number.format(value, {places: 1, locale: "en-us"});
	        };
	
	        var serviceURL = config.operationalMapService.replace("MapServer", "FeatureServer");
	        var heatmapFeatureLayerOptions = {
	          mode: FeatureLayer.MODE_SNAPSHOT,
	          outFields: ["weight"],
			  opacity: "0.6"
	        };
	        heatmapFeatureLayer = new FeatureLayer(config.operationalMapService + "/" + config.hotSpotLayerID, heatmapFeatureLayerOptions);
	 		//heatmapFeatureLayer.setDefinitionExpression("centerid in (" + selectedCenterID + ")");
	       
	        var blurCtrl = document.getElementById("blurControl");
	        var maxCtrl = document.getElementById("maxControl");
	        var minCtrl = document.getElementById("minControl");
	        var valCtrl = document.getElementById("valueControl");
	
	        
	         var heatmapRenderer = new HeatmapRenderer({
	          colors: ["rgba(0, 0, 255, 0)","rgb(0, 0, 255)","rgb(255,237,125)", "rgb(255, 0, 0)"],
			  field: "weight",
	          blurRadius: blurCtrl.value,
	          // maxPixelIntensity: maxCtrl.value,
	          // minPixelIntensity: minCtrl.value
	        }); 
	        heatmapFeatureLayer.setRenderer(heatmapRenderer);
	        map.addLayer(heatmapFeatureLayer);
	        heatmapFeatureLayer.hide();
	        /** Add event handlers for interactivity **/
	
	        var sliders = document.querySelectorAll(".blurInfo p~input[type=range]");
	        var addLiveValue = function (ctrl){
	          var val = ctrl.previousElementSibling.querySelector("span");
	          ctrl.addEventListener("input", function (evt){
	            val.innerHTML = evt.target.value;
	          });
	        };
	        for (var i = 0; i < sliders.length; i++) {
	          addLiveValue(sliders.item(i));
	        }
	
	        blurCtrl.addEventListener("change", function (evt){
	          var r = +evt.target.value;
	          if (r !== heatmapRenderer.blurRadius) {
	            heatmapRenderer.blurRadius = r;
	            heatmapFeatureLayer.redraw();
	          }
	        });
	        // maxCtrl.addEventListener("change", function (evt){
	        //   var r = +evt.target.value;
	        //   if (r !== heatmapRenderer.maxPixelIntensity) {
	        //     heatmapRenderer.maxPixelIntensity = r;
	        //     heatmapFeatureLayer.redraw();
	        //   }
	        // });
	        // minCtrl.addEventListener("change", function (evt){
	        //   var r = +evt.target.value;
	        //   if (r !== heatmapRenderer.minPixelIntensity) {
	        //     heatmapRenderer.minPixelIntensity = r;
	        //     heatmapFeatureLayer.redraw();
	        //   }
	        // });
	        // --------------------------------------------------------------------
	        // When check / uncheck the control for the HeatmapRenderer field,
	        // we will leave the blurRadius and the minPixelIntensity values the
	        // same. However we will adjust the maxPixelIntensity value so it
	        // spreads the colors across the range of magnitude values. For your
	        // own dataset, you will need to experiment to find what looks good
	        // based upon the level of geography when you display the heatmap
	        // and the values in your dataset.
	        // --------------------------------------------------------------------
	        /*valCtrl.addEventListener("change", function (evt){
	          var chk = evt.target.checked;
	          if (!chk) {
	            document.getElementById("maxValue").innerHTML = 56;
	            maxCtrl.value = 21;
	            heatmapRenderer.maxPixelIntensity = 56;
	          }
	          else {
	            document.getElementById("maxValue").innerHTML = 100;
	            maxCtrl.value = 100;
	            heatmapRenderer.maxPixelIntensity =100;
	
	          }
	          heatmapRenderer.field = (chk) ? "weight" : null;
	          heatmapFeatureLayer.redraw();
	        });*/
	 	}
	 }     
});