define([
  "rok/config",
  "esri/map",
  "esri/layers/ArcGISDynamicMapServiceLayer",
  "esri/layers/FeatureLayer",
  "esri/InfoTemplate",
  "esri/dijit/Popup",
  "esri/dijit/PopupTemplate",
  "esri/dijit/PopupMobile",
  "ncam/PopupExtended",
  "esri/layers/DynamicLayerInfo",
  "esri/layers/LayerDataSource",
  "esri/layers/LayerDrawingOptions",
  "esri/layers/TableDataSource",
  "esri/Color",
  "esri/renderers/SimpleRenderer",
  "esri/renderers/UniqueValueRenderer",
  "esri/renderers/ClassBreaksRenderer",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",
], function (
  config,
  Map,
  ArcGISDynamicMapServiceLayer,
  FeatureLayer,
  InfoTemplate,
  Popup,
  PopupTemplate,
  PopupMobile,
  PopupExtended,
  DynamicLayerInfo,
  LayerDataSource,
  LayerDrawingOptions,
  TableDataSource,
  Color,
  SimpleRenderer,
  UniqueValueRenderer,
  ClassBreaksRenderer,
  SimpleFillSymbol,
  SimpleLineSymbol
) {
  var fillSymbolC = new SimpleFillSymbol();
  var fillSymbolD = new SimpleFillSymbol();
  var fillSymbolE = new SimpleFillSymbol();
  function uniqueArr(arr) {
    var uniqueArrOfCenters = [];
    $.each(arr, function (i, el) {
      if ($.inArray(el, uniqueArrOfCenters) === -1) uniqueArrOfCenters.push(el);
    });
    return uniqueArrOfCenters;
  }
  return {
    createCompetitorOverlayLayer: function (
      map,
      dynamicMapService,
      tableName,
      compsforRenderer,
      centersForRenderer,
      compTadeAreaColorList
    ) {
      // update global object with layer info
      var layerName = "trutrade_replication.dbo." + tableName; //"Simon.DBO.TempBlkGrpDevices";
      // create a table data source to access the lakes layer
      var dataSource = new TableDataSource();
      dataSource.workspaceId = "MyDatabaseWorkspaceID"; // not exposed via REST :(
      dataSource.dataSourceName = layerName;
      // and now a layer source
      var layerSource = new LayerDataSource();
      layerSource.dataSource = dataSource;
      //Create Renderer
      fillSymbolC.setColor(new Color([135, 206, 250]));
      fillSymbolC.setStyle("solid");
      fillSymbolC.setOutline(null);
      fillSymbolD.setColor(new Color([0, 0, 139]));
      fillSymbolD.setStyle("solid");
      fillSymbolD.setOutline(null);

      fillSymbolE.setColor(new Color([255, 170, 0]));
      fillSymbolE.setStyle("solid");
      fillSymbolE.setOutline(null);
      //var cbr = new SimpleRenderer(fillSymbolC);

      var cbr = new UniqueValueRenderer(null, "LegendValue");
      cbr.addValue({
        value: "1",
        symbol: fillSymbolC,
        label: "Competitor Overlay",
      });
      cbr.addValue({
        value: "2",
        symbol: fillSymbolD,
        label: "Competitor Only",
      });
      cbr.addValue({
        value: "3",
        symbol: fillSymbolE,
        label: "Location Origin",
      });
      //Create Feature layer and Infowindow
      var infoTemplate = new PopupTemplate({
        title: "Highest Device Count",
        fieldInfos: [
          { fieldName: "CenterName", label: "Center", visible: true },
          { fieldName: "TotalDevices", label: "Devices Seen", visible: true },
          { fieldName: "GeoID", label: "GeoID", visible: true },
        ],
        extended: {
          themeClass: "extended", //uses a pretty bad custom theme defined in PopupExtended.css.
          scaleSelected: 0,
        },
      });

      competitorOverlayFL = new FeatureLayer(
        config.operationalMapService + "/dynamicLayer",
        {
          mode: FeatureLayer.MODE_ONDEMAND,
          outFields: ["*"],
          objectIdField: "ObjectID",
          source: layerSource,
          infoTemplate: infoTemplate,
          id: "competitorOverlayLayer",
          opacity: dynamicMapService.opacity,
          visible: false,
        }
      );
      competitorOverlayFL.setRenderer(cbr);
      competitorOverlayFL.setAutoGeneralize(false);
      map.addLayer(competitorOverlayFL, 0);
      competitorOverlayFL.on("load", function (e) {
        $("#competitorOverlayButton").button("reset");
        $("#competitorOverlayChkDiv").show();
      });
    },
    showCompetitorOverlayLegend: function () {
      legend.refresh([{ layer: competitorOverlayFL, title: "Overlay" }]);
    },
  };
});
