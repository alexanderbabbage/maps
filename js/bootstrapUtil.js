/*
* bootstrapUtil.js
* -------
* Copyright 2014 Patrick Engineering Inc. All rights reserved.
*/

define(
	[

	],
	function () {
		'use strict'; // ECMAScript 5 Strict Mode

		return {
			getMediaMode: function() {
				// http://stackoverflow.com/questions/14441456/how-to-detect-which-device-view-youre-on-using-twitter-bootstrap-api
				var $el;
				var envs = ['xs', 'sm', 'md', 'lg'];

				$el = $('<div>');
				$el.appendTo($('body'));

				for (var i = envs.length - 1; i >= 0; i--) {
					var env = envs[i];

					$el.addClass('hidden-'+env);
					if ($el.is(':hidden')) {
						$el.remove();
						return env;
					}
				}
			},
			toggleButtonLoading: function($btn, isLoading) {
				// changes button to loading state and sets color to same red
				// as btn-danger.  Not removing/adding classes since difficult to tell
				// which initial btn- class the button had.
				var btnDangerRed = '#d9534f';
				if (isLoading) {
					$btn.button('loading');
					$btn.css('color', 'white');
					$btn.css('border-color', 'red');
					$btn.css('background-color', btnDangerRed);
				} else {
					$btn.button('reset');
					$btn.css('color', '');
					$btn.css('border-color', '');
					$btn.css('background-color', '');
				}
			}
		};
	}
	);
