define({
  //Default configuration settings for the applciation. This is where you"ll define things like a bing maps key,
  //default web map, default app color theme and more. These values can be overwritten by template configuration settings
  //and url parameters.
  center: [-98.883, 38.421],
  zoom: 4,
  appTitle: "Alexander Babbage",
  logo: "img/Pinpoint_44px_White.png", //alexander-babbage-logo.gif
  operationalMapService:
    "https://maps.alexanderbabbage.com/arcgis/rest/services/trutrade/TruTrade_Main_RDS/MapServer",
  centerLayerID: 46,
  competitorCenterLayerID: 45,
  centerRadiusLayerID: 47,
  hotSpotLayerID: 52,
  geofences: 43,
  standardLayerIDs: [46, 45, 50, 49, 51, 48, 43],
  demographicLayerIDs: [2, 3, 10, 11, 12, 40],
  /*"competitorRadiusLayerID": 4,*/
  centerShopperOrigins: 50,
  competitorShopperOrigins: 49,
  centerMarketPenetration: 51,
  competitorMarketPenetration: 48,
  marketShare: 20,
  printTaskURL:
    "https://arcgis4.roktech.net/arcgis/rest/services/ROKPrint/ExportWebMapV2/GPServer/Export%20Web%20Map",
  radioTVLayers:
    "https://arcgis4.roktech.net/arcgis/rest/services/alexanderbabbage/RadioTV_Layers/MapServer",
  tvID: 0,
  radioID: 1,
  tvCoverageID: 2,
  radioCoverageID: 3,
  blockGroupID: 4,
  competitorOverlay:
    "https://maps.alexanderbabbage.com/arcgis/rest/services/trutrade/TruTrade_Main/MapServer/17",
});
