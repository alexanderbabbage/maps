define([
  "rok/config",
  "esri/map",
  "esri/layers/ArcGISDynamicMapServiceLayer",
  "esri/layers/FeatureLayer",
  "esri/InfoTemplate",
  "esri/dijit/Popup",
  "esri/dijit/PopupTemplate",
  "esri/dijit/PopupMobile",
  "ncam/PopupExtended",
  "esri/layers/DynamicLayerInfo",
  "esri/layers/LayerDataSource",
  "esri/layers/LayerDrawingOptions",
  "esri/layers/TableDataSource",
  "esri/Color",
  "esri/renderers/SimpleRenderer",
  "esri/renderers/UniqueValueRenderer",
  "esri/renderers/ClassBreaksRenderer",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",
], function (
  config,
  Map,
  ArcGISDynamicMapServiceLayer,
  FeatureLayer,
  InfoTemplate,
  Popup,
  PopupTemplate,
  PopupMobile,
  PopupExtended,
  DynamicLayerInfo,
  LayerDataSource,
  LayerDrawingOptions,
  TableDataSource,
  Color,
  SimpleRenderer,
  UniqueValueRenderer,
  ClassBreaksRenderer,
  SimpleFillSymbol,
  SimpleLineSymbol
) {
  var fillSymbolC = new SimpleFillSymbol();
  function uniqueArr(arr) {
    var uniqueArrOfCenters = [];
    $.each(arr, function (i, el) {
      if ($.inArray(el, uniqueArrOfCenters) === -1) uniqueArrOfCenters.push(el);
    });
    return uniqueArrOfCenters;
  }
  return {
    createShopperOriginLayer: function (
      map,
      dynamicMapService,
      tableName,
      compsforRenderer,
      centersForRenderer,
      compTadeAreaColorList
    ) {
      var layerName = "trutrade_replication.dbo." + tableName; //"Simon.DBO.TempBlkGrpDevices";
      // create a table data source to access the lakes layer
      var dataSource = new TableDataSource();
      dataSource.workspaceId = "MyDatabaseWorkspaceID"; // not exposed via REST :(
      dataSource.dataSourceName = layerName;
      // and now a layer source
      var layerSource = new LayerDataSource();
      layerSource.dataSource = dataSource;
      //Create Renderer
      centersForRenderer = uniqueArr(centersForRenderer);
      var cbr = new UniqueValueRenderer("", "CenterName");
      for (var j = 0; j < centersForRenderer.length; j++) {
        var compVal = centersForRenderer[j].replace("''", "'");
        var symbolC;
        var redColor = Color.fromHex(compTadeAreaColorList[j]);
        fillSymbolC.setColor(new Color(redColor));
        fillSymbolC.setStyle("solid");
        symbolC = fillSymbolC;
        cbr.addValue({
          value: compVal,
          label: compVal,
          description: null,
          symbol: new SimpleFillSymbol()
            .setColor(new Color(redColor))
            .setOutline(new SimpleLineSymbol().setWidth(0)),
        });
      }
      for (var j = 0; j < compsforRenderer.length; j++) {
        var compVal = compsforRenderer[j].replace("''", "'");
        var symbolC;
        var k = 1 + j;
        var redColor = Color.fromHex(compTadeAreaColorList[k]);
        fillSymbolC.setColor(new Color(redColor));
        fillSymbolC.setStyle("solid");
        symbolC = fillSymbolC;
        cbr.addValue({
          value: compVal,
          label: compVal,
          description: null,
          symbol: new SimpleFillSymbol()
            .setColor(new Color(redColor))
            .setOutline(new SimpleLineSymbol().setWidth(0)),
        });
      }
      //Create Feature layer and Infowindow
      var content =
        "<b>Total Devices</b>: ${TotalDevices}" +
        "<br /><b>GeoID</b>: ${GeoID}";
      var infoTemplate = new PopupTemplate({
        title: "Highest Device Count",
        fieldInfos: [
          { fieldName: "CenterName", label: "Center", visible: true },
          { fieldName: "TotalDevices", label: "Devices Seen", visible: true },
          { fieldName: "GeoID", label: "GeoID", visible: true },
        ],
        extended: {
          themeClass: "extended", //uses a pretty bad custom theme defined in PopupExtended.css.
          scaleSelected: 0,
        },
      });
      shopperOriginFL = new FeatureLayer(
        config.operationalMapService + "/dynamicLayer",
        {
          mode: FeatureLayer.MODE_ONDEMAND,
          outFields: ["*"],
          objectIdField: "ObjectID",
          source: layerSource,
          infoTemplate: infoTemplate,
          id: "originLayer",
          opacity: dynamicMapService.opacity,
          visible: false,
        }
      );
      shopperOriginFL.setRenderer(cbr);
      shopperOriginFL.setAutoGeneralize(false);
      map.addLayer(shopperOriginFL, 0);
      shopperOriginFL.on("load", function (e) {
        $("#blkGroupButton").button("reset");
        $("#blkGroupChkDiv").show();
      });
    },
    showBlockGroupLegend: function () {
      legend.refresh([
        { layer: map._layers.originLayer, title: "Block Group Winner" },
      ]);
    },
  };
});
